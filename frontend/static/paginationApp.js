async function displayPagination() {
    let totalPages = JSON.parse(localStorage.getItem('pageOrgList')).totalPages

    if (totalPages > 1) {
        let container = document.getElementById('searchOrgResultContainer')
        let divPaginationContainer = document.createElement('div')
        divPaginationContainer.setAttribute("class", "paginationContainer")

        let divProgressContainer = document.createElement('div')
        divProgressContainer.setAttribute("class", "progress-container")

        container.insertAdjacentElement("beforeend", divPaginationContainer)
        divPaginationContainer.insertAdjacentElement("beforeend", divProgressContainer)

        let divProgress = document.createElement('div')
        divProgress.setAttribute("class", "progress")
        divProgress.setAttribute("id", "progress")

        let divCircle1 = document.createElement('div')
        divCircle1.setAttribute("class", "circle active")
        divCircle1.textContent = 1

        divProgressContainer.insertAdjacentElement("beforeend", divProgress)
        divProgressContainer.insertAdjacentElement("beforeend", divCircle1)

        for (let i = 2; i <= totalPages; i++) {
            let divCircle = document.createElement('div')
            divCircle.setAttribute("class", "circle")
            divCircle.textContent = i
            divProgressContainer.insertAdjacentElement("beforeend", divCircle)
        }

        let divPaginationButtons = document.createElement('div')
        divPaginationButtons.setAttribute("class", "paginationButtons")

        let buttonPrev = document.createElement('div')
        buttonPrev.setAttribute("class", "button")
        buttonPrev.setAttribute("id", "previous")
        buttonPrev.textContent = '<<'
        //buttonPrev.disabled = true
        buttonPrev.setAttribute("disabled", "disabled")

        let buttonNext = document.createElement('div')
        buttonNext.setAttribute("class", "button")
        buttonNext.setAttribute("id", "next")
        buttonNext.textContent = '>>'

        divPaginationContainer.insertAdjacentElement("beforeend", divPaginationButtons)
        divPaginationButtons.insertAdjacentElement("beforeend", buttonPrev)
        divPaginationButtons.insertAdjacentElement("beforeend", buttonNext)

        await activatePagination()
    }
}

async function activatePagination() {
    const progress = document.getElementById('progress');
    const prev = document.getElementById('previous');
    const next = document.getElementById('next');
    const circles = document.querySelectorAll('.circle');

    let currentActive = 1;

    next.addEventListener('click', async () => {
        currentActive++;
        if (currentActive > circles.length) {
            currentActive = circles.length;
        }
        await update();
    });

    prev.addEventListener('click', async () => {
        currentActive--;
        if (currentActive < 1) {
            currentActive = 1;
        }
        await update();
    })

    async function update() {
        circles.forEach((circle, idx) => {
            if (idx < currentActive) {
                circle.classList.add('active');
            } else {
                circle.classList.remove('active');
            }
        });
        const actives = document.querySelectorAll('.active');

        progress.style.width = (actives.length - 1) / (circles.length - 1) * 100 + '%';

        if (currentActive === 1) {
            prev.disabled = true;
            next.disabled = false
        } else if (currentActive === circles.length) {
            next.disabled = true;
            prev.disabled = false
        } else {
            prev.disabled = false;
            next.disabled = false;
        }
        let currentPage = currentActive - 1
        let updatedUrl = localStorage.getItem('url') + '?pageNo=' + currentPage + '&pageSize=' + localStorage.getItem("defaultUnits")
        await searchOrganizationsByCriteria(updatedUrl, localStorage.getItem('organizationCountry'),
            localStorage.getItem('organizationType'))
    }
}

