async function sendRegistrationData(urlParam, formParam) {
    formParam.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(formParam)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(urlParam, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage()
            }
        });
        if (response.ok) {
            let user = await response.json()
            let userName = JSON.parse(JSON.stringify(user)).username
            alert(localize('userText') + userName + localize('successRegText'))
            document.location = '/loginPage.html'
        } else if (response.status === 400) {
            let json = await response.json()
            let validationMessages = JSON.stringify(json)
            validationAddForm(validationMessages)
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

async function sendLoginData(urlParam, formParam, replaceUrl) {
    formParam.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(formParam)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(urlParam, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage()
            }
        });
        if (response.ok) {
            let json = await response.json()
            let authResult = JSON.parse(JSON.stringify(json))
            let token = authResult.token
            let userName = authResult.username
            let bearerToken = 'Bearer_' + token
            localStorage.setItem('bearerToken', bearerToken)
            alert(localize('userText') + userName + localize('successLoginText'))
            document.location = replaceUrl
        } else if (response.status === 400 || 403) {
            loginValidation()
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

async function fetchOrganizationData(urlParam, formParam, urlReplaceParam) {
    formParam.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(formParam)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value);
        const response = await fetch(urlParam, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage(),
                'Authorization': localStorage.getItem('bearerToken')
            }
        });
        if (response.ok) {
            let json = await response.json()
            let organization = JSON.stringify(json)
            localStorage.setItem('currentOrganization', organization)
            document.location = urlReplaceParam
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else if (response.status === 400) {
            let json = await response.json()
            let validationMessages = JSON.stringify(json)
            validationAddForm(validationMessages)
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

async function fetchContactData(urlParam, formParam, urlReplaceParam) {
    formParam.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(formParam)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(urlParam, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage(),
                'Authorization': localStorage.getItem('bearerToken')
            }
        });
        if (response.ok) {
            let json = await response.json()
            let contact = JSON.parse(JSON.stringify(json))
            let contactName = contact.name
            localStorage.setItem("currentContactName", contactName)
            document.location = urlReplaceParam
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else if (response.status === 400) {
            let json = await response.json()
            let validationMessages = JSON.stringify(json)
            validationAddForm(validationMessages)
        } else {
            alert("HTTP error: " + response.status)
        }
    };
}

function onLoadContactPage() {
    let organization = JSON.parse(localStorage.getItem('currentOrganization'))
    let organizationId = organization.id
    let organizationName = organization.name
    let form = document.forms.namedItem('addContactForm')
    form.elements.namedItem('organizationId').value = organizationId
    let orgName = document.getElementById('orgName')
    orgName.innerText = localize('addingContactHeaderText')
    orgName.append(organizationName.trim() + ':')
    addContactInfo()
}

function onLoadFurtherContactPage() {
    let organization = JSON.parse(localStorage.getItem("currentOrganization"))
    let currentContactName = localStorage.getItem("currentContactName")
    let form = document.forms.namedItem('addContactForm')
    form.elements.namedItem('organizationId').value = organization.id
    let contName = document.getElementById('contName')
    contName.innerText = localize('youAddedText')
    contName.append(currentContactName + '.')

    let buttonRevertToOrganization = document.getElementById('revertToCurrentOrg')
    buttonRevertToOrganization.setAttribute('href', '/organizationAndContactDetails.html?orgId=' + organization.id)
    buttonRevertToOrganization.innerText = localize('revertToCurrentOrgText')

    let addNewOrg = document.getElementById('addNewOrg')
    addNewOrg.innerText = localize('addNewOrgText')

    let orgName = document.getElementById('orgName')
    orgName.innerText = localize('addAnotherContactText')
    orgName.append(organization.name + ':')

    addContactInfo()
}

function addContactInfo() {
    let inputContactName = document.getElementById('contactName')
    inputContactName.setAttribute('placeholder', localize('inputContactNameText'))

    let contactType = document.getElementById('contactType')
    let h6СontactTypeHeader = document.createElement('h6')
    h6СontactTypeHeader.innerText = localize('сontactTypeHeaderText')
    contactType.insertAdjacentElement("beforebegin", h6СontactTypeHeader)

    let contactDirection = document.getElementById('direction')
    let h6СontactDirectionHeader = document.createElement('h6')
    h6СontactDirectionHeader.innerText = localize('сontactDirectionHeaderText')
    contactDirection.insertAdjacentElement("beforebegin", h6СontactDirectionHeader)

    let contactOfficePhoneNumber = document.getElementById('contactOfficePhoneNumber')
    contactOfficePhoneNumber.setAttribute('placeholder', localize('contactOfficePhoneNumberText'))

    let contactMobilePhoneNumber = document.getElementById('contactMobilePhoneNumber')
    contactMobilePhoneNumber.setAttribute('placeholder', localize('contactMobilePhoneNumberText'))

    let contactEmail = document.getElementById('contactEmail')
    contactEmail.setAttribute('placeholder', localize('contactEmailText'))

    let contactComments = document.getElementById('contactComments')
    contactComments.setAttribute('placeholder', localize('contactCommentsText'))

    let submitContact = document.getElementById('submitContact')
    submitContact.innerText = localize('submitContactText')
}

async function fetchUserAuthStatus() {
    const url = '/api/authStatus'
    let response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        const userStatus = await response.text()
        localStorage.setItem('userStatus', userStatus)
        const element = await document.getElementById('currentUserName')
        let userStatusName = userStatus
        if (userStatus === 'anonymous') {
            userStatusName = localize('userStatusName')
            localStorage.setItem('bearerToken', null)
        }
        element.textContent = localize('userStatus') + userStatusName
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function getTypeListForAdding(url, type) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let typeArray = []
        typeArray = await response.json()
        let selectTypeObjForAdding = document.getElementById(type)
        let typeSize = typeArray.length
        let selectTypeSize = 0
        for (let i = 0; i < typeSize; i++) {
            let exactType = typeArray[i]
            if (selectTypeObjForAdding) {
                selectTypeObjForAdding.options[selectTypeSize++] = new Option(exactType, exactType)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function getTypeListForSearching(url, type) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let typeArray = []
        typeArray = await response.json()
        let selectTypeObjForSearching = document.getElementById(type)
        let typeSize = typeArray.length
        let selectTypeSize = 1
        for (let i = 0; i < typeSize; i++) {
            let exactType = typeArray[i]
            if (selectTypeObjForSearching) {
                selectTypeObjForSearching.options[selectTypeSize++] = new Option(exactType, exactType)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function getCountryListForSearching(url, organizationCountry) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let countryArray = await response.json()
        let selectCountryObject = document.getElementById(organizationCountry)
        let countrySize = countryArray.length
        let selectCountrySize = 1;
        for (let i = 0; i < countrySize; i++) {
            let exactCountry = countryArray[i]
            if (selectCountryObject) {
                selectCountryObject.options[selectCountrySize++] = new Option(exactCountry, exactCountry)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function getCountryListForAdding(url, organizationCountry) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let countryArray = await response.json()
        let selectCountryObject = document.getElementById(organizationCountry)
        let countrySize = countryArray.length
        let selectCountrySize = 0;
        for (let i = 0; i < countrySize; i++) {
            let exactCountry = countryArray[i]
            if (selectCountryObject) {
                selectCountryObject.options[selectCountrySize++] = new Option(exactCountry, exactCountry)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function getDirectionList(url, direction) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let directions = await response.json()
        let selectDirectionObject = document.getElementById(direction)
        let directionSize = directions.length
        let selectDirectionSize = 0;
        for (let i = 0; i < directionSize; i++) {
            let exactDirection = directions[i]
            if (selectDirectionObject) {
                selectDirectionObject.options[selectDirectionSize++] = new Option(exactDirection, exactDirection)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

function onLoadIndexPage() {
    let welcomeToMakShipping = document.getElementById('welcomeToMakShipping')
    welcomeToMakShipping.innerText = localize('welcomeToMakShippingText')

    let descriptionOfApp = document.getElementById('descriptionOfApp')
    descriptionOfApp.innerText = localize('descriptionOfAppText')

    let clickAddingOrgPageInMenu = document.getElementById('clickAddingOrgPageInMenu')
    clickAddingOrgPageInMenu.innerText = localize('clickAddingOrgPageInMenuText')

    let clickSearchingOrgPageInMenu = document.getElementById('clickSearchingOrgPageInMenu')
    clickSearchingOrgPageInMenu.innerText = localize('clickSearchingOrgPageInMenuText')
}

function onLoadAboutApp() {
    let makShippingAppHeader = document.getElementById('makShippingAppHeader')
    makShippingAppHeader.innerText = localize('makShippingAppHeaderText')

    let descriptionOfAppInAboutApp = document.getElementById('descriptionOfAppInAboutApp')
    descriptionOfAppInAboutApp.innerText = localize('descriptionOfAppText')

    let findUsingApp = document.getElementById('findUsingApp')
    findUsingApp.innerText = localize('findUsingAppText')

    let airService = document.getElementById('airService')
    airService.innerText = localize('airServiceText')

    let seaService = document.getElementById('seaService')
    seaService.innerText = localize('seaServiceText')

    let truckService = document.getElementById('truckService')
    truckService.innerText = localize('truckServiceText')

    let railwayService = document.getElementById('railwayService')
    railwayService.innerText = localize('railwayServiceText')

    let lclService = document.getElementById('lclService')
    lclService.innerText = localize('lclServiceText')

    let terminalService = document.getElementById('terminalService')
    terminalService.innerText = localize('terminalServiceText')

    let customsService = document.getElementById('customsService')
    customsService.innerText = localize('customsServiceText')

    let forwarderService = document.getElementById('forwarderService')
    forwarderService.innerText = localize('forwarderServiceText')

    let clickAddingOrgPageInMenu = document.getElementById('clickAddingOrgPageInAboutApp')
    clickAddingOrgPageInMenu.innerText = localize('clickAddingOrgPageInMenuText')

    let clickSearchingOrgPageInMenu = document.getElementById('clickSearchingOrgPageInAboutApp')
    clickSearchingOrgPageInMenu.innerText = localize('clickSearchingOrgPageInMenuText')
}

function onLoadLoginPage() {
    let logFormHeader = document.querySelector('.logFormHeader')
    logFormHeader.innerText = localize('loginHeaderText')

    let usernameLabel = document.querySelector('#usernameLabel')
    usernameLabel.innerText = localize('usernameLabelText')

    let passwordLabel = document.querySelector('#passwordLabel')
    passwordLabel.innerText = localize('passwordLabelText')

    let loginButtonInForm = document.querySelector('#loginButtonInForm')
    loginButtonInForm.innerText = localize('loginButtonInFormText')

    let doNotYouHaveAccount = document.querySelector('#doNotYouHaveAccount')
    doNotYouHaveAccount.innerText = localize('doNotYouHaveAccountText')

    let createAccountButton = document.querySelector('#createAccountButton')
    createAccountButton.innerText = localize('createAccountButtonText')
}

function onLoadRegistrationPage() {
    let registerFormHeader = document.querySelector('.registerFormHeader')
    registerFormHeader.innerText = localize('regHeaderText')

    let usernameLabel = document.querySelector('#usernameLabel')
    usernameLabel.innerText = localize('usernameLabelText')

    let emailLabel = document.getElementById('emailLabel')
    emailLabel.innerText = localize('emailLabelText')

    let passwordLabel = document.querySelector('#passwordLabel')
    passwordLabel.innerText = localize('passwordLabelText')

    let regButtonInForm = document.getElementById('registerButtonInForm')
    regButtonInForm.innerText = localize('regButtonInFormText')
}

function beforeOnLoadAddOrganizationPage() {
    let userStatus = localStorage.getItem('userStatus')
    if (userStatus === 'anonymous') {
        document.location.href = '/loginPage.html'
    } else {
        window.location.href = 'addOrganizations.html'
    }
}

function onLoadAddOrganizationPage() {
    let mainDiv = document.getElementById('addOrgPage')
    let h1AddOrgHeader = document.createElement('h2')
    h1AddOrgHeader.innerText = localize('addOrgHeaderText')
    mainDiv.insertAdjacentElement("afterbegin", h1AddOrgHeader)

    let organizationNamePlaceHolder = document.getElementById('organizationName')
    organizationNamePlaceHolder.setAttribute('placeholder', localize('organizationNamePlaceHolderText'))

    let organizationCountry = document.getElementById('organizationCountryForAdding')
    let h6OrgCountryHeader = document.createElement('h6')
    h6OrgCountryHeader.innerText = localize('selectOrganizationCountryText')
    organizationCountry.insertAdjacentElement("beforebegin", h6OrgCountryHeader)

    let organizationType = document.getElementById('organizationTypeForAddingOrganization')
    let h6OrganizationTypeHeader = document.createElement('h6')
    h6OrganizationTypeHeader.innerText = localize('selectOrganizationTypeText')
    organizationType.insertAdjacentElement("beforebegin", h6OrganizationTypeHeader)

    let organizationAddressPlaceHolder = document.getElementById('address')
    organizationAddressPlaceHolder.setAttribute('placeholder', localize('organizationAddressPlaceHolderText'))

    let organizationPhoneNumberPlaceHolder = document.getElementById('organizationPhoneNumber')
    organizationPhoneNumberPlaceHolder.setAttribute('placeholder', localize('organizationPhoneNumberPlaceHolderText'))

    let organizationWebsitePlaceHolder = document.getElementById('website')
    organizationWebsitePlaceHolder.setAttribute('placeholder', localize('organizationWebsitePlaceHolderText'))

    let organizationCommentsPlaceHolder = document.getElementById('organizationComments')
    organizationCommentsPlaceHolder.setAttribute('placeholder', localize('organizationCommentsPlaceHolderText'))

    let btnAddOrg = document.getElementById('btnAddOrg')
    btnAddOrg.innerText = localize('btnAddOrgText')
}

getTypeListForAdding('/api/organizationTypeList', 'organizationTypeForAddingOrganization')
getTypeListForSearching('/api/organizationTypeList', 'organizationTypeForSearching')
getTypeListForAdding('/api/contactTypeList', 'contactType')
getCountryListForSearching('/api/countryListFromExistingOrganizations', 'organizationCountryForSearching')
getCountryListForAdding('/api/countryList', 'organizationCountryForAdding')
getDirectionList('/api/getDirectionList', 'direction')
