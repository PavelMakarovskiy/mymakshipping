function onLoadEditOrganization() {
    let currentOrganization = JSON.parse(localStorage.getItem('currentOrganization'))
    let form = document.forms.namedItem('editOrgForm')
    let name = form.elements.namedItem('organizationName')
    uploadEditOrgLocaleText()
    getListForEdit('/api/countryList', 'organizationCountryForEditOrganization',
        currentOrganization.country)
    getListForEdit('/api/organizationTypeList', 'organizationTypeForEditOrganization',
        currentOrganization.organizationType)
    let address = form.elements.namedItem('address')
    let phoneNumber = form.elements.namedItem('organizationPhoneNumber')
    let website = form.elements.namedItem('website')
    let comments = form.elements.namedItem('organizationComments')
    name.value = currentOrganization.name
    address.value = currentOrganization.address
    phoneNumber.value = currentOrganization.phoneNumber
    website.value = currentOrganization.website
    comments.value = currentOrganization.comments
}

function uploadEditOrgLocaleText() {
    let editOrgDetailsHeader = document.getElementById('editOrgDetailsHeader')
    editOrgDetailsHeader.innerText = localize('editOrgDetailsHeaderText')

    let editName = document.getElementById('editName')
    editName.innerText = localize('editNameText')

    let editSelectCountryOrg = document.getElementById('editSelectCountryOrg')
    editSelectCountryOrg.innerText = localize('selectOrganizationCountryText')

    let editSelectTypeOrg = document.getElementById('editSelectTypeOrg')
    editSelectTypeOrg.innerText = localize('selectOrganizationTypeText')

    let editOrgAddress = document.getElementById('editOrgAddress')
    editOrgAddress.innerText = localize('editOrgAddressText')

    let editPhoneNumber = document.getElementById('editPhoneNumber')
    editPhoneNumber.innerText = localize('editPhoneNumberText')

    let editWebSite = document.getElementById('editWebSite')
    editWebSite.innerText = localize('editWebSiteText')

    let editComments = document.getElementById('editComments')
    editComments.innerText = localize('editCommentsText')

    let updateOrgDetails = document.getElementById('updateOrgDetails')
    updateOrgDetails.innerText = localize('updateOrgDetailsText')
}

async function onLoadEditContactPage() {
    let contactId = document.location.href.split('?')[1].split('=')[1]
    localStorage.setItem('currentContactId', contactId)
    let response = await fetch('/api/getContact?contactId=' + contactId, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        let currentContact = JSON.parse(JSON.stringify(json))
        uploadEditContactPageText()
        let orgName = JSON.parse(localStorage.getItem('currentOrganization')).name
        document.getElementById('orgName').append(orgName + ':')
        let form = document.forms.namedItem('editContactForm')
        form.elements.namedItem('contactName').value = currentContact.name
        form.elements.namedItem('contactOfficePhoneNumber').value = currentContact.officePhoneNumber
        form.elements.namedItem('contactMobilePhoneNumber').value = currentContact.mobilePhoneNumber
        form.elements.namedItem('contactEmail').value = currentContact.email
        form.elements.namedItem('contactComments').value = currentContact.comments
        getListForEdit('/api/contactTypeList', 'contactType',
            currentContact.contactType)
        getListForEdit('/api/getDirectionList', 'direction',
            currentContact.direction)
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert('HTTP error: ' + response.status)
    }
}

function uploadEditContactPageText() {
    let editContactPageOrgNameHeader = document.getElementById('orgName')
    editContactPageOrgNameHeader.innerText = localize('editContactPageOrgNameHeaderText')

    let editContName = document.getElementById('editContName')
    editContName.innerText = localize('editContNameText')

    let editSelectType = document.getElementById('editSelectType')
    editSelectType.innerText = localize('сontactTypeHeaderText')

    let editSelectDirection = document.getElementById('editSelectDirection')
    editSelectDirection.innerText = localize('сontactDirectionHeaderText')

    let editOfficePhone = document.getElementById('editOfficePhone')
    editOfficePhone.innerText = localize('editOfficePhoneText')

    let editMobilePhone = document.getElementById('editMobilePhone')
    editMobilePhone.innerText = localize('editMobilePhoneText')

    let editEmail = document.getElementById('editEmail')
    editEmail.innerText = localize('editEmailText')

    let editComments = document.getElementById('editComments')
    editComments.innerText = localize('editCommentsText')

    let updateContact = document.getElementById('updateContact')
    updateContact.innerText = localize('updateContactDetailsText')
}

async function updateOrganizationDetails(url, form, link) {
    let currentOrganization = JSON.parse(localStorage.getItem('currentOrganization'))
    let orgId = currentOrganization.id
    form.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(form)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(url + '?orgId=' + orgId, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage(),
                'Authorization': localStorage.getItem('bearerToken')
            }
        });
        if (response.ok) {
            document.location.href = link + '?orgId=' + orgId
            alert(localize('detailsOfText') + currentOrganization.name + localize('successUpdate'))
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            let json = await response.json()
            let validationMessages = JSON.stringify(json)
            validationAddForm(validationMessages)
        }
    }
}

async function updateContactDetails(url, form, link) {
    let contactId = localStorage.getItem('currentContactId')
    form.onsubmit = async (e) => {
        e.preventDefault()
        let obj = {}
        let formData = new FormData(form)
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(url + '?contactId=' + contactId, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Accept-Language': getLanguage(),
                'Authorization': localStorage.getItem('bearerToken')
            }
        });
        if (response.ok) {
            let json = await response.json()
            let contact = JSON.parse(JSON.stringify(json))
            let orgId = contact.organization.id
            document.location.href = link + '?orgId=' + orgId
            alert(localize('detailsOfText') + localize('successUpdate'))
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            let json = await response.json()
            let validationMessages = JSON.stringify(json)
            validationAddForm(validationMessages)
        }
    }
}

async function getListForEdit(url, id, selectedValue) {
    const response = await fetch(url, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let array = await response.json()
        let selectTypeObjForEdit = document.getElementById(id)
        let typeSize = array.length
        let selectTypeSize = 0
        for (let i = 0; i < typeSize; i++) {
            let exactType = array[i]
            if (exactType === selectedValue) {
                let index = selectTypeSize++
                selectTypeObjForEdit.options[index] = new Option(exactType, exactType)
                selectTypeObjForEdit.options[index].selected = true
            } else {
                selectTypeObjForEdit.options[selectTypeSize++] = new Option(exactType, exactType)
            }
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert('HTML error: ' + response.status)
    }
}

async function removeOrganization() {
    let orgId = JSON.parse(localStorage.getItem('currentOrganization')).id
    let orgName = JSON.parse(localStorage.getItem('currentOrganization')).name
    if (confirmDelete(localize('wouldYouLineRemoveCompany') + orgName + '?')) {
        let response = await fetch('/api/removeOrganization', {
            method: 'POST',
            headers: {
                'Authorization': localStorage.getItem('bearerToken'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(orgId)
        })
        let boolean = await response.text()
        if (response.ok && boolean === 'true') {
            document.location.href = '/searchOrganizations.html'
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            alert('HTML errors: ' + response.status)
        }
    } else {
        document.location.reload()
    }
}

async function removeContact(contactId, contactName) {
    if (confirmDelete(localize('wouldYouLineRemoveContact') + contactName + '?')) {
        let orgId = JSON.parse(localStorage.getItem('currentOrganization')).id
        let response = await fetch('/api/removeContact', {
            method: 'POST',
            headers: {
                'Authorization': localStorage.getItem('bearerToken'),
                'Content-Type': 'Application/json'
            },
            body: JSON.stringify(contactId)
        })
        if (response.ok && await response.text() === 'true') {
            document.location.href = '/organizationAndContactDetails.html?orgId=' + orgId
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            alert('HTML error: ' + response.status)
        }
    } else {
        document.location.reload()
    }
}

function confirmDelete(text) {
    return confirm(text)
}