async function onLoadHeader() {
    logInOrOut()

    getLanguage()

    let homePageInMenuText = document.getElementById('homePageInMenuText')
    homePageInMenuText.innerText = localize('homePageInMenuText')

    let aboutAppInMenuText = document.getElementById('aboutAppInMenuText')
    aboutAppInMenuText.innerText = localize('aboutAppInMenuText')

    let searchingOrgPageInMenuText = document.getElementById('searchingOrgPageInMenuText')
    searchingOrgPageInMenuText.innerText = localize('searchingOrgPageInMenuText')

    let addingOrgPageInMenuText = document.getElementById('addingOrgPageInMenuText')
    addingOrgPageInMenuText.innerText = localize('addingOrgPageInMenuText')

    localeClick()
}

function localeClick() {
    document.querySelector('.ru').onclick = async function () {
        localStorage.setItem('lang', 'ru')
        window.location.reload()
    }

    document.querySelector('.en').onclick = async function () {
        localStorage.setItem('lang', 'en')
        window.location.reload()
    }
}

async function logInOrOut() {
    await fetchUserAuthStatus()
    let userStatus = localStorage.getItem('userStatus')
    if (userStatus === 'anonymous') {
        let url = '/loginPage.html'
        let className = 'logButton'
        createLogButton(localize('logInButton'), className)
        document.querySelector('.' + className).onclick = async function () {
            document.location = url
        }
    } else {
        let className = 'logButton'
        createLogButton(localize('logOutButton'), className)
        activateLogout()
    }
}

function createLogButton(content, className) {
    let logInButton = document.querySelector('.' + className)
    logInButton.textContent = content
}

function activateLogout() {
    document.querySelector('.logButton').onclick = async function () {
        const response = await fetch('/api/logOut', {
            method: 'POST',
            headers: {
                'Authorization': localStorage.getItem('bearerToken')
            }
        })
        if (response.ok) {
            let json = await response.json()
            let token = JSON.parse(JSON.stringify(json)).token
            localStorage.setItem('bearerToken', token)
            document.location = 'index.html'
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

const menuIcon = document.querySelector('.menuIcon')
if (menuIcon) {
    const menuFrame = document.querySelector('.menuFrame')
    const menu = document.querySelector('.menu')
    const menuContent = document.querySelectorAll('.menuContent')
    const header = document.querySelector('.header')
    const langInMenu = document.querySelector('#langInMenu')
    const ru = document.querySelector('.ru')
    const en = document.querySelector('.en')
    menuIcon.addEventListener("click", function (e) {
        menuIcon.classList.toggle('active');
        menuFrame.classList.toggle('active');
        langInMenu.classList.toggle('active')
        menu.classList.toggle('active');
        for (let i = 0; i < menuContent.length; i++) {
            menuContent[i].classList.toggle('active');
        }
        header.classList.toggle('active');
        ru.addEventListener("click", function (e) {
            localStorage.setItem('lang', 'ru')
            window.location.reload()
        });
        en.addEventListener("click", function (e) {
            localStorage.setItem('lang', 'en')
            window.location.reload()
        });
    });
}

const userIcon = document.querySelector('.userIcon')
if (userIcon) {
    const rightSideHeader = document.querySelector('.rightSideHeader')
    const currentUserName = document.querySelector('#currentUserName')
    const userInfoClose = document.querySelector('.userInfoClose')
    userIcon.addEventListener("click", function () {
        rightSideHeader.classList.toggle('active')
        currentUserName.classList.toggle('active')
        userInfoClose.classList.toggle('active')
        userInfoClose.addEventListener("click", function () {
            rightSideHeader.classList.remove('active')
            currentUserName.classList.remove('active')
            userInfoClose.classList.remove('active')
        })
    })
}

const ruInMenu = document.querySelector('.ruInMenu')
if (ruInMenu) {
    ruInMenu.addEventListener("click", function () {
        localStorage.setItem('lang', 'ru')
        window.location.reload()
    })
}

const enInMenu = document.querySelector('.enInMenu')
if (enInMenu) {
    enInMenu.addEventListener("click", function () {
        localStorage.setItem('lang', 'en')
        window.location.reload()
    })
}

adminRequest()

async function adminRequest() {
    let adminName = localStorage.getItem('userStatus')
    const response = await fetch('/api/adminRequest?adminName=' + adminName, {
        method: 'POST',
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        let value = JSON.parse(JSON.stringify(json))
        if (value === true) {
            showAdminButton()
        }
    } else {
        alert("HTTP error: " + response.status)
    }
}

function showAdminButton() {
    let logButton = document.querySelector('.logButton')
    let adminButton = document.createElement('button')
    adminButton.setAttribute('class', 'adminButton')
    adminButton.innerText = 'A'
    logButton.insertAdjacentElement("afterend", adminButton)
    adminButton.onclick = function () {
        document.location = '/adminFunctions.html'
    }
}

async function onLoadingAdminFunctions() {
    const response = await fetch('/api/adminFunctions', {
        method: 'POST',
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        let userList = JSON.parse(JSON.stringify(json))
        createUsersTable(userList)
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function createUsersTable(userList) {
    let adminMainPageFrame = document.querySelector('.adminMainPageFrame')
    for (let i = 0; i < userList.length; i++) {
        let form = document.createElement('form')
        let formName = 'editUserDetailsForm' + i
        form.setAttribute('name', formName)
        form.setAttribute('id', formName)
        form.setAttribute('class', 'editUserDetailsForm')
        adminMainPageFrame.insertAdjacentElement("beforeend", form)
        let notes = document.createElement('div')
        form.insertAdjacentElement("beforeend", notes)
        let hEditUserName = document.createElement('h')
        hEditUserName.setAttribute('class', 'firstColumn')
        hEditUserName.setAttribute('id', 'editUserNameText')
        hEditUserName.innerText = 'Name of user: '
        notes.insertAdjacentElement("afterend", hEditUserName)
        let inputUsername = document.createElement('input')
        inputUsername.type = 'text'
        inputUsername.name = 'username'
        inputUsername.value = userList[i].username
        hEditUserName.insertAdjacentElement("afterend", inputUsername)
        form.appendChild(inputUsername)

        let hUserRole = document.createElement('h')
        hUserRole.setAttribute('class', 'firstColumn')
        hUserRole.setAttribute('id', 'userRoleText')
        hUserRole.innerText = 'Roles: '
        inputUsername.insertAdjacentElement("afterend", hUserRole)

        let inputRoles = document.createElement('input')
        inputRoles.name = 'roles'

        for (let y = 0; y < userList[i].roles.length; y++) {
            let inputRoleIdText = document.createElement('h')
            inputRoleIdText.innerText = 'Role ID:'
            let inputRoleId = document.createElement('input')
            let inputRole = document.createElement('input')
            inputRoleId.name = 'roleId' + y
            inputRoleId.value = userList[i].roles[y].roleId
            inputRole.type = 'text'
            inputRole.name = 'roleName' + y
            inputRole.value = userList[i].roles[y].roleName
            hUserRole.insertAdjacentElement("afterend", inputRoleIdText)
            inputRoleIdText.insertAdjacentElement("afterend", inputRoleId)
            inputRoleId.insertAdjacentElement("afterend", inputRole)
            form.appendChild(inputRoleIdText)
            form.appendChild(inputRoleId)
            form.appendChild(inputRole)
        }

        let hEditStatus = document.createElement('h')
        hEditStatus.setAttribute('class', 'firstColumn')
        hEditStatus.setAttribute('id', 'editStatusText')
        hEditStatus.innerText = 'User status active: '
        form.insertAdjacentElement("beforeend", hEditStatus)
        let inputUserStatus = document.createElement('input')
        inputUserStatus.type = 'text'
        inputUserStatus.name = 'isActive'
        inputUserStatus.value = userList[i].active
        hEditStatus.insertAdjacentElement("afterend", inputUserStatus)
        form.appendChild(inputUserStatus)

        let hEditId = document.createElement('h')
        hEditId.setAttribute('class', 'firstColumn')
        hEditId.setAttribute('id', 'editIdText')
        hEditId.innerText = 'User id: '
        form.insertAdjacentElement("beforeend", hEditId)
        let inputUserId = document.createElement('input')
        inputUserId.type = 'text'
        inputUserId.name = 'id'
        inputUserId.value = userList[i].id
        hEditId.insertAdjacentElement("afterend", inputUserId)
        form.appendChild(inputUserId)

        let hEditEmail = document.createElement('h')
        hEditEmail.setAttribute('class', 'firstColumn')
        hEditEmail.setAttribute('id', 'editEmailText')
        hEditEmail.innerText = 'Email: '
        form.insertAdjacentElement("beforeend", hEditEmail)
        let inputUserEmail = document.createElement('input')
        inputUserEmail.type = 'text'
        inputUserEmail.name = 'email'
        inputUserEmail.value = userList[i].email
        hEditEmail.insertAdjacentElement("afterend", inputUserEmail)
        form.appendChild(inputUserEmail)

        let hCreated = document.createElement('h')
        hCreated.setAttribute('class', 'firstColumn')
        hCreated.setAttribute('id', 'createdText')
        hCreated.innerText = 'Date of creation: '
        form.insertAdjacentElement("beforeend", hCreated)
        let userCreationDate = document.createElement('h')
        userCreationDate.innerText = userList[i].created
        hCreated.insertAdjacentElement("afterend", userCreationDate)

        let userButtonAdmin = document.createElement('button')
        userButtonAdmin.setAttribute('class', 'userButtonAdmin')
        userButtonAdmin.setAttribute('type', 'submit')
        userButtonAdmin.innerText = 'SUBMIT'
        form.insertAdjacentElement("beforeend", userButtonAdmin)

        userButtonAdmin.onclick = function () {
            let url = '/api/updateUserDetails'
            let currentForm = document.getElementById(formName)
            let link = '/api/adminFunctions.html'
            currentForm.submit()
            updateUserDetails(url, currentForm, link, userList[i].username)
        }
    }

    async function updateUserDetails(url, form, link, username) {
        form.onsubmit = async (e) => {
            e.preventDefault()
            let formData = new FormData(form)
            let obj = {}
            let roles = {}
            let role = {}
            let i = 0
            formData.forEach((value, key) => {
                let number = Number.parseInt(key.toString().slice(-1))
                if (key.substr(0, 4) === 'role' && number === i) {
                    role[key.toString().slice(0, -1)] = value
                    let roleSize = Object.keys(role).length
                    if (roleSize === 2) {
                        roles[i++] = role
                        role = {}
                    }
                } else {
                    obj[key] = value
                }
            })

            obj.roles = roles
            const response = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(obj),
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': getLanguage(),
                    'Authorization': localStorage.getItem('bearerToken')
                }
            });
            if (response.ok) {
                document.location.href = link
                alert('User ' + username + ' updated.')
            } else {
                let json = await response.json()
                let validationMessages = JSON.stringify(json)
                validationAddForm(validationMessages)
            }
        }
    }
}





