function validationAddForm(validationMessages) {
    let v = JSON.parse(validationMessages)
    let entries = Object.entries(v)
    let oldElements = Array.from(document.getElementsByClassName("notes"))
    if (oldElements.length !== 0) {
        for (let element of oldElements) {
            element.innerHTML = ''
        }
    }
    for (let entry of entries) {
        let key = entry[0].toString()
        let value = entry[1]
        let idName = "note" + key
        localStorage.setItem("idName", idName)
        if (document.getElementById(idName) == null) {
            let note = document.getElementById(idName)
            note.innerText = value.toString()
        } else {
            document.getElementById(idName).innerText = ""
            document.getElementById(idName).innerText = value.toString()
        }
    }
}

function loginValidation() {
    let oldElements = document.getElementsByClassName("notes")
    if (oldElements.length !== 0) {
        for (let element of oldElements) {
            element.innerText = ''
        }
    }
    let note = document.querySelector('#loginNote')
    note.innerText = localize('loginFailedText')
}