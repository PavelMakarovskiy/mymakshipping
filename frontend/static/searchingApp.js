async function searchOrganizationsByCriteriaByDefault(url, form) {
    let defaultPage = 0;
    let defaultUnits = 24;
    localStorage.setItem("url", url)
    localStorage.setItem("organizationCountry", form.organizationCountry.value)
    localStorage.setItem("organizationType", form.organizationType.value)
    localStorage.setItem("defaultUnits", defaultUnits)
    let defaultUrl = url + '?pageNo=' + defaultPage + '&pageSize=' + defaultUnits;
    form.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(form)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(defaultUrl, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('bearerToken'),
            }
        });
        if (response.ok) {
            let json = await response.json()
            localStorage.setItem('pageOrgList', JSON.stringify(json))
            document.location.href = '/searchOrganizationsResult.html'
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

async function searchOrganizationsByCriteria(url, organizationCountry, organizationType) {
    let form = new FormData()
    form.append("organizationCountry", organizationCountry)
    form.append("organizationType", organizationType)
    let obj = {}
    form.forEach((value, key) => obj[key] = value)
    const response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(obj),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('bearerToken'),
        }
    });
    if (response.ok) {
        let json = await response.json()
        localStorage.setItem('pageOrgList', JSON.stringify(json))
        let pageOrgList = JSON.parse(localStorage.getItem('pageOrgList'))
        let orgList = pageOrgList.content
        let container = document.getElementById('resultContainer')
        container.innerHTML = ""

        let h4Head = document.createElement('h4')
        h4Head.innerHTML = localize('resultByNameText')
        container.insertAdjacentElement("beforeend", h4Head)

        for (let i = 0; i < orgList.length; i++) {
            outputShortOrganizationDetailsList(orgList[i], container)
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function onLoadSearchOrganizationsResult() {
    let pageOrgList = JSON.parse(localStorage.getItem('pageOrgList'))
    let orgList = pageOrgList.content
    let container = document.getElementById('resultContainer')

    let h4Head = document.createElement('h4')
    h4Head.innerHTML = localize('resultByNameText')
    container.insertAdjacentElement("beforeend", h4Head)

    for (let i = 0; i < orgList.length; i++) {
        outputShortOrganizationDetailsList(orgList[i], container)
    }
    await displayPagination()
}

async function onLoadOrganizationAndContactDetails() {
    let query = window.location.href.split('?')[1]
    let orgId = query.split('=')[1]
    localStorage.setItem('organizationId', orgId)
    const response = await fetch('/api/getOrganization?orgId=' + orgId, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        localStorage.setItem('currentOrganization', JSON.stringify(json))
        await seeOrganizationDetails(JSON.parse(JSON.stringify(json)))
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function seeOrganizationDetails(organization) {
    let allPageContainer = document.getElementById('allPageContainer')
    let h3Header = document.createElement('h3')
    h3Header.setAttribute('id', 'orgDetailsHeader')
    h3Header.innerHTML = localize('detailsCompanyHeaderText')
    allPageContainer.insertAdjacentElement('afterbegin', h3Header)


    let nameText = document.getElementById('nameText')
    nameText.innerText = localize('nameText')
    let orgName = document.getElementById('orgName')
    orgName.innerText = organization.name

    let typeText = document.getElementById('typeText')
    typeText.innerText = localize('typeText')
    let orgType = document.getElementById('orgType')
    orgType.innerText = organization.organizationType

    let countryText = document.getElementById('countryText')
    countryText.innerText = localize('countryText')
    let orgCountry = document.getElementById('orgCountry')
    orgCountry.innerText = organization.country

    let webText = document.getElementById('webText')
    webText.innerText = localize('webText')
    let orgWebsite = document.getElementById('orgWebsite')
    orgWebsite.innerText = organization.website

    let phoneText = document.getElementById('phoneText')
    phoneText.innerText = localize('phoneText')
    let orgPhoneNumber = document.getElementById('orgPhoneNumber')
    orgPhoneNumber.innerText = organization.phoneNumber

    let addressText = document.getElementById('addressText')
    addressText.innerText = localize('addressText')
    let orgAddress = document.getElementById('orgAddress')
    orgAddress.innerText = organization.address

    let commentText = document.getElementById('commentText')
    commentText.innerText = localize('commentText')
    let orgComments = document.getElementById('orgComments')
    orgComments.innerText = organization.comments

    let btnSeeAllContacts = document.getElementById('seeAllContacts')
    btnSeeAllContacts.innerText = localize('btnSeeAllContactsText')

    let btnFindContactsByPosition = document.getElementById('findContactsByPosition')
    btnFindContactsByPosition.innerText = localize('btnFindContactsByPositionText')

    let btnAddNewContact = document.getElementById('addNewContact')
    btnAddNewContact.innerText = localize('btnAddNewContactText')

    let btnEditCompanyDetails = document.getElementById('editCompanyDetails')
    btnEditCompanyDetails.innerText = localize('btnEditCompanyDetailsText')

    let btnRemoveCompany = document.getElementById('removeCompany')
    btnRemoveCompany.innerText = localize('btnRemoveCompanyText')
}

async function seeAllContactsDetails() {
    let orgId = localStorage.getItem('organizationId')
    const response = await fetch('/api/getContacts?orgId=' + orgId, {
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        let contacts = JSON.parse(JSON.stringify(json))
        if (contacts.length > 0) {
            await seeContactsDetails(contacts)
        } else {
            await seeNullContacts()
        }
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert("HTTP error: " + response.status)
    }
}

async function seeContactsDetailsByPosition(url, formPosition) {
    let orgId = localStorage.getItem('organizationId')
    let urlWithParam = url + '?orgId=' + orgId
    formPosition.onsubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData(formPosition)
        let obj = {}
        formData.forEach((value, key) => obj[key] = value)
        const response = await fetch(urlWithParam, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('bearerToken'),
            }
        });
        if (response.ok) {
            let json = await response.json()
            let contacts = JSON.parse(JSON.stringify(json))
            if (contacts.length > 0) {
                await seeContactsDetails(contacts)
            } else {
                await seeNullContacts()
            }
        } else if (response.status === 403) {
            document.location.href = '/loginPage.html'
        } else {
            alert("HTTP error: " + response.status)
        }
    }
}

async function seeContactsDetails(contacts) {
    let contactsHead = document.getElementById('contactsHead')
    contactsHead.innerHTML = localize('contactsHeadText')
    let resultContactsContainer = document.getElementById('resultContactsContainer')
    resultContactsContainer.innerText = ''

    for (let i = 0; i < contacts.length; i++) {
        let contactId = await contacts[i].id

        let container = document.createElement('div')
        container.setAttribute('id', 'contactDetails')
        resultContactsContainer.insertAdjacentElement("beforeend", container)

        let table = document.createElement('table')
        container.insertAdjacentElement("beforeend", table)
        let tr1 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr1)
        let th11 = document.createElement('th')
        th11.setAttribute('class', 'firstColumn')
        th11.setAttribute('id', 'positionText')
        th11.innerText = localize('positionText')
        tr1.insertAdjacentElement("beforeend", th11)
        let th12 = document.createElement('th')
        th12.setAttribute('class', 'secondColumn')
        th12.setAttribute('id', 'contactType')
        th12.innerText = contacts[i].contactType
        tr1.insertAdjacentElement("beforeend", th12)

        let tr2 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr2)
        let th21 = document.createElement('th')
        th21.setAttribute('class', 'firstColumn')
        th21.setAttribute('id', 'nameText')
        th21.innerText = localize('nameText')
        tr2.insertAdjacentElement("beforeend", th21)
        let th22 = document.createElement('th')
        th22.setAttribute('class', 'secondColumn')
        th22.setAttribute('id', 'contactName')
        th22.innerText = contacts[i].name
        tr2.insertAdjacentElement("beforeend", th22)

        let tr3 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr3)
        let th31 = document.createElement('th')
        th31.setAttribute('class', 'firstColumn')
        th31.setAttribute('id', 'areaOfWorkText')
        th31.innerText = localize('areaOfWorkText')
        tr3.insertAdjacentElement("beforeend", th31)
        let th32 = document.createElement('th')
        th32.setAttribute('class', 'secondColumn')
        th32.setAttribute('id', 'contactDirection')
        th32.innerText = contacts[i].direction
        tr3.insertAdjacentElement("beforeend", th32)

        let tr4 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr4)
        let th41 = document.createElement('th')
        th41.setAttribute('class', 'firstColumn')
        th41.setAttribute('id', 'emailLabelText')
        th41.innerText = localize('emailLabelText')
        tr4.insertAdjacentElement("beforeend", th41)
        let th42 = document.createElement('th')
        th42.setAttribute('class', 'secondColumn')
        th42.setAttribute('id', 'contactEmail')
        th42.innerText = contacts[i].email
        tr4.insertAdjacentElement("beforeend", th42)

        let tr5 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr5)
        let th51 = document.createElement('th')
        th51.setAttribute('class', 'firstColumn')
        th51.setAttribute('id', 'officePhoneNumber')
        th51.innerText = localize('officePhoneNumber')
        tr5.insertAdjacentElement("beforeend", th51)
        let th52 = document.createElement('th')
        th52.setAttribute('class', 'secondColumn')
        th52.setAttribute('id', 'contactOfficePhoneNumber')
        th52.innerText = contacts[i].officePhoneNumber
        tr5.insertAdjacentElement("beforeend", th52)

        let tr6 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr6)
        let th61 = document.createElement('th')
        th61.setAttribute('class', 'firstColumn')
        th61.setAttribute('id', 'mobilePhoneNumber')
        th61.innerText = localize('mobilePhoneNumber')
        tr6.insertAdjacentElement("beforeend", th61)
        let th62 = document.createElement('th')
        th62.setAttribute('class', 'secondColumn')
        th62.setAttribute('id', 'contactMobilePhoneNumber')
        th62.innerText = contacts[i].mobilePhoneNumber
        tr6.insertAdjacentElement("beforeend", th62)

        let tr7 = document.createElement('tr')
        table.insertAdjacentElement("beforeend", tr7)
        let th71 = document.createElement('th')
        th71.setAttribute('class', 'firstColumn')
        th71.setAttribute('id', 'commentText')
        th71.innerText = localize('commentText')
        tr7.insertAdjacentElement("beforeend", th71)
        let th72 = document.createElement('th')
        th72.setAttribute('class', 'secondColumn')
        th72.setAttribute('id', 'contactComment')
        th72.innerText = contacts[i].comments
        tr7.insertAdjacentElement("beforeend", th72)

        let contactButtons = document.createElement('div')
        contactButtons.setAttribute('id', 'contactButtons')
        container.insertAdjacentElement("beforeend", contactButtons)

        let buttonLinkEdit = document.createElement('button')
        buttonLinkEdit.onclick = function () {
            location.href = '/editContact.html?contactId=' + contacts[i].id
        }
        buttonLinkEdit.setAttribute('id', 'contactEditButton')
        buttonLinkEdit.setAttribute('class', 'contactDetailsButton')
        buttonLinkEdit.textContent = localize('editContactDetails')
        contactButtons.insertAdjacentElement("beforeend", buttonLinkEdit)

        let buttonLinkRemove = document.createElement('button')
        buttonLinkRemove.onclick = function () {
            removeContact(contactId, contacts[i].name)
        }
        buttonLinkRemove.setAttribute('id', 'contactRemoveButton')
        buttonLinkRemove.setAttribute('class', 'contactDetailsButton')
        buttonLinkRemove.textContent = localize('removeContact')
        contactButtons.insertAdjacentElement("beforeend", buttonLinkRemove)
    }
}

async function seeNullContacts() {
    let contactsHead = document.getElementById('contactsHead')
    contactsHead.innerHTML = ''
    contactsHead.innerHTML = localize('contactsHeadText')
    let resultContactsContainer = document.getElementById('resultContactsContainer')
    resultContactsContainer.innerHTML = ''
    let h4 = document.createElement('h4')
    h4.innerHTML = localize('noContactText')
    resultContactsContainer.insertAdjacentElement("beforeend", h4)

    let buttonAdd = document.createElement('a')
    buttonAdd.setAttribute('href', 'addContact.html')
    buttonAdd.setAttribute('class', 'btn btn-success')
    buttonAdd.textContent = localize('btnAddNewContactText')
    resultContactsContainer.insertAdjacentElement("beforeend", buttonAdd)
}

if (document.querySelector('#searchOrganizationByName') != null) {
    document.querySelector('#searchOrganizationByName').oninput = async function () {
        let val = this.value.toLowerCase().trim()
        let organizationList = await getAllOrganizations()
        let result = document.getElementById('searchingOrganizationResult')
        result.innerHTML = ''
        if (val !== '') {
            let h4Head = document.createElement('h4')
            h4Head.innerHTML = localize('resultByNameText')
            result.insertAdjacentElement("beforeend", h4Head)

            for (let i = 0; i < organizationList.length; i++) {
                if (organizationList[i].name.toLowerCase().search(val) === -1) {
                    // let p = document.createElement('p')
                    // p.innerHTML = 'NO RESULT'
                    // result.insertAdjacentElement("beforeend", p)
                } else {
                    await outputShortOrganizationDetailsList(organizationList[i], result)
                }
            }
        }
    }
}

async function getAllOrganizations() {
    let response = await fetch('/api/getAllOrganizations', {
        method: 'POST',
        headers: {
            'Authorization': localStorage.getItem('bearerToken')
        }
    })
    if (response.ok) {
        let json = await response.json()
        return JSON.parse(JSON.stringify(json))
    } else if (response.status === 403) {
        document.location.href = '/loginPage.html'
    } else {
        alert('HTML error: ' + response.status)
    }
}

function outputShortOrganizationDetailsList(organization, container) {
    let div = document.createElement('div')
    div.setAttribute('class', 'organizationDetails')
    container.insertAdjacentElement("beforeend", div)

    let h5 = document.createElement('h5')
    h5.innerHTML = organization.name
    div.insertAdjacentElement("beforeend", h5)

    let h6Type = document.createElement('h6')
    h6Type.innerHTML = localize('organizationTypeText') + organization.organizationType
    div.insertAdjacentElement("beforeend", h6Type)

    let h6Country = document.createElement('h6')
    h6Country.innerHTML = localize('organizationCountryText') + organization.country
    div.insertAdjacentElement("beforeend", h6Country)

    let buttonLink = document.createElement('a')
    buttonLink.setAttribute('href', '/organizationAndContactDetails.html?orgId=' + organization.id)
    buttonLink.setAttribute('class', 'btn btn-success')
    buttonLink.textContent = localize('buttonSeeDetailsText')
    div.insertAdjacentElement("beforeend", buttonLink)
}

function beforeOnLoadingSearchOrganizationPage() {
    let userStatus = localStorage.getItem('userStatus')
    if (userStatus === 'anonymous') {
        document.location.href = '/loginPage.html'
    } else {
        window.location.href = '/searchOrganizations.html'
    }
}

function onLoadSearchOrganizationPage() {
    let orgSearchingHeader = document.querySelector('.orgSearchingHeader')
    orgSearchingHeader.innerText = localize('searchOrgHeader')

    let orgSearchByCountryAndTypeHeader = document.querySelector('.orgSearchByCountryAndTypeHeader')
    orgSearchByCountryAndTypeHeader.innerText = localize('searchByCountryAndTypeHeader')

    let organizationCountryLabel = document.querySelector('#organizationCountryLabel')
    organizationCountryLabel.innerText = localize('selectOrgCountry')
    let optionValue = document.getElementById('optionValue')
    optionValue.innerText = localize('optionValueText')

    let organizationTypeLabel = document.querySelector('#organizationTypeLabel')
    organizationTypeLabel.innerText = localize('selectOrgType')
    let optionValueType = document.getElementById('optionValueType')
    optionValueType.innerText = localize('optionValueTypeText')

    let searchOrgButton = document.querySelector('#searchOrgButton')
    searchOrgButton.innerText = localize('searchOrgButtonText')

    let organizationSearchByNameLabel = document.querySelector('#organizationSearchByNameLabel')
    organizationSearchByNameLabel.innerText = localize('searchOrgByName')

    let searchOrganizationByName = document.querySelector('#searchOrganizationByName')
    searchOrganizationByName.setAttribute('placeholder', localize('inputNamePlaceHolder'))
}

