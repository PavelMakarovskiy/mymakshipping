## Production

1. Build and start all services:
```bash
docker-compose up -d --build
```
2. The application is ready on http://localhost

## Development

1. Start local environment:
```bash
docker-compose -f docker-compose.local.yml up -d
```
2. Start API service:
```bash
./mvnw spring-boot:run
```
3. The application is ready on http://localhost