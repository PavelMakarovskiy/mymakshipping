FROM maven:3.8-openjdk-11 AS builder
WORKDIR /opt/mymakshipping-builder/
COPY . .
RUN mvn package -Dmaven.test.skip

FROM openjdk:11-jre-slim
WORKDIR /opt/mymakshipping/
COPY --from=builder /opt/mymakshipping-builder/target/ /opt/mymakshipping/
ENTRYPOINT ["java","-jar","mymakshipping-0.0.1-SNAPSHOT.jar"]
