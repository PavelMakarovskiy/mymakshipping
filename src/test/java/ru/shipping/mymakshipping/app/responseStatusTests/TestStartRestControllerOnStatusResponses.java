package ru.shipping.mymakshipping.app.responseStatusTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:applicationTest.properties")
@Transactional
@WebAppConfiguration
public class TestStartRestControllerOnStatusResponses {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void configure() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void callRegisterWithValidArgumentsThenIsCreatedStatus() throws Exception {
        String requestBody = "{\"username\":\"Eric\",\"email\":\"eric@gmail.com\",\"password\":\"12345678\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/registrationPage")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "EN"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void callRegisterWitIncorrectPasswordFormatThenIs4xxClientError() throws Exception {
        String requestBody = "{\"username\":\"Eric\",\"email\":\"eric@gmail.com\",\"password\":\"1234\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/registrationPage")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "EN"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callRegisterWithNullArgumentsThenIs4xxClientError() throws Exception {
        String requestBody = "";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/registrationPage")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "EN"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callRegisterWithExistingNameThenIs4xxClientError() throws Exception {
        User user = new User();
        user.setUsername("Eric");
        user.setEmail("erics@gmail.com");
        user.setPassword("123456783");

        userRepository.save(user);

        String requestBody = "{\"username\":\"Eric\",\"email\":\"eric@gmail.com\",\"password\":\"12345678\"}";

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/registrationPage")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Accept-Language", "EN"))
                        .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callGetAuthStatusThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
        .get("/authStatus")
        .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callGetOrganizationTypeListThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/organizationTypeList")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callGetContactTypeListThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/contactTypeList")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callGetCountriesFromExistingOrganizationsThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/countryListFromExistingOrganizations")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callGetCountriesThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/countryList")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callGetDirectionsThenResponseIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/getDirectionList")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
