package ru.shipping.mymakshipping.app.responseStatusTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.shipping.mymakshipping.app.entities.role.ERole;
import ru.shipping.mymakshipping.app.entities.role.Role;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.UserRepository;
import ru.shipping.mymakshipping.app.security.AuthenticationResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:applicationTest.properties")
@WebAppConfiguration
@Transactional
public class TestAuthenticationControllerLogin {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    AuthenticationResult authenticationResult;

    private User user;

    @Before
    public void configure() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        user = new User();
        user.setUsername("Eric");
        user.setEmail("erics@gmail.com");
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRoleName(ERole.ROLE_USER.name());
        roles.add(role);
        user.setRoles(roles);
        user.setCreated(new Date());
        user.setActive(true);
    }

    @Test
    public void callLoginWithValidArgumentsThenIsOk() throws Exception {
        user.setPassword(passwordEncoder.encode("12345678"));
        userRepository.save(user);

        String requestBody = "{\"username\":\"Eric\",\"password\":\"12345678\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/loginPage")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .header("Accept-Language", "en"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void callLoginWithWrongPasswordThenBadCredentialsException() throws Exception {
        user.setPassword(passwordEncoder.encode("vvv12345678"));
        userRepository.save(user);

        String requestBody = "{\"username\":\"Eric\",\"password\":\"12345678\"}";

        Throwable thrown = catchThrowable(() -> {
            mockMvc.perform(MockMvcRequestBuilders
                    .post("/loginPage")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestBody)
                    .header("Accept-Language", "en"));
        });
        assertThat(thrown).hasCauseExactlyInstanceOf(BadCredentialsException.class);
        assertThat(thrown).hasMessageContaining("Incorrect name of user or password!");
    }

    @Test
    public void callLoginWithNullArgumentsThenBadCredentialsIs4xxClientError() throws Exception {
        user.setPassword(passwordEncoder.encode("vvv12345678"));
        userRepository.save(user);

        String requestBody = "{\"username\":\"\",\"password\":\"\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/loginPage")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .header("Accept-Language", "en"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callLogoutThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/logOut")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{username: null, token: null}"));
    }
}
