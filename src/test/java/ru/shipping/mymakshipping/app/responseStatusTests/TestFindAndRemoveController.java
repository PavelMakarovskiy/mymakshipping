package ru.shipping.mymakshipping.app.responseStatusTests;

import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:applicationTest.properties")
@Transactional
public class TestFindAndRemoveController {
    private MockMvc mockMvc;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Organization organization;

    private Contact contact;

    @Before
    public void configure() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        organization = new Organization();
        contact = new Contact();
        organization.setName("Dinamo");
        organization.setId(1);
        organization.setCountry("Russia");
        organization.setOrganizationType("FORWARDER");
        contact.setName("Monica");
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        contact.setOrganization(organization);
        organizationRepository.save(organization);
        contactRepository.save(contact);
    }

    @Test
    public void callGetOrganizationWithValidIdThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/getOrganization?orgId=1")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callGetOrganizationWithInvalidIdThenThrowNotFoundException() {
        Throwable thrown = catchThrowable(() -> {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/getOrganization?orgId=22222")
                    .header("Authorization", "Bearer token"));
        });
        assertThat(thrown).hasCauseExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void callGetContactsWithValidIdThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/getContacts?orgId=1")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callGetContactsWithInValidIdThenIsNotFoundException() {
        Throwable thrown = catchThrowable(() -> {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/getContacts?orgId=22222")
                    .header("Authorization", "Bearer token"));
        });
        assertThat(thrown).hasCauseExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void callGetContactWithValidIdThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/getContact?contactId=2")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callGetContactWithInValidIdThenIsNotFoundException() {
        Throwable thrown = catchThrowable(() -> {
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/getContact?contactId=22222")
                    .header("Authorization", "Bearer token"));
        });
        assertThat(thrown).hasCauseExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void callGetOrganizationsByCountryAndTypeWithValidParametersThenIsOk() throws Exception {
        String requestBody = "{\"organizationCountry\":\"Russia\",\"organizationType\":\"Any type\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/searchOrganizations?pageNo=0&pageSize=1")
                .content(requestBody)
                .header("Authorization", "Bearer token")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callSeeContactsDetailsByPositionWithValidParametersThenIsOk() throws Exception {
        String requestBody = "{\"contactType\":\"MANAGER\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/seeContactsDetailsByPosition?orgId=1")
                .content(requestBody)
                .header("Authorization", "Bearer token")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callGetAllOrganizationsWithValidParametersThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/getAllOrganizations")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callRemoveOrganizationThenIsOk() throws Exception {
        contactRepository.deleteById(2L);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/removeOrganization")
                .content("1")
                .header("Authorization", "Bearer token")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callRemoveContactThenIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/removeContact")
                .content("2")
                .header("Authorization", "Bearer token")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

}
