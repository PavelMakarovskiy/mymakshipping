package ru.shipping.mymakshipping.app.responseStatusTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:applicationTest.properties")
@Transactional
public class TestAddAndUpdateController {
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private ContactRepository contactRepository;

    private Organization organization;

    private Contact contact;

    @Before
    public void configure() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        organization = new Organization();
        contact = new Contact();
        organization.setName("Wert");
        organization.setOrganizationType("SHIPPING LINE");
        organization.setCountry("Germany");
        contact.setName("Monica");

    }

    @Test
    public void callAddOrganizationWithValidArgumentsThenIsOk() throws Exception {
        String requestBody = "{\"organizationName\":\"Wert\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"89112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addOrganization")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callAddOrganizationWhenNameIsNullThenIs4xxError() throws Exception {
        String requestBody = "{\"organizationName\":\"\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"89112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addOrganization")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

    }

    @Test
    public void callAddOrganizationWithIncorrectPhoneNumberFormatThenIs4xxError() throws Exception {
        String requestBody = "{\"organizationName\":\"Wert\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"8A9112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addOrganization")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddOrganizationWithExistingOrgNameThenIs4xxError() throws Exception {
        organizationRepository.save(organization);

        String requestBody = "{\"organizationName\":\"Wert\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"89112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addOrganization")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithValidArgumentsThenIsOk() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callAddContactWithNullNameThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithNullTypeThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithNullDirectionThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"MANAGER\",\"direction\":\"\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithWithIncorrectOfficePhoneNumberFormatThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112Q497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithWithIncorrectMobilePhoneNumberFormatThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101 add (909)\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callAddContactWithWithIncorrectEmailFormatThenIs4xxClientError() throws Exception {
        String requestBody = "{\"contactName\":\"Jao\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"wcom\",\"contactComments\":\"air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/addContact")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callUpdateOrganizationWithValidArgumentsThenIsOk() throws Exception {
        organization.setId(1);
        organizationRepository.save(organization);

        String requestBody = "{\"organizationName\":\"Werts\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"89112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateOrganization?orgId=1")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callUpdateOrganizationWhenNameIsNullThenIs4xxError() throws Exception {
        organization.setId(1);
        organizationRepository.save(organization);

        String requestBody = "{\"organizationName\":\"\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"89112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateOrganization?orgId=1")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

    }

    @Test
    public void callUpdateOrganizationWithIncorrectPhoneNumberFormatThenIs4xxError() throws Exception {
        organization.setId(1);
        organizationRepository.save(organization);

        String requestBody = "{\"organizationName\":\"Wert\",\"organizationCountry\":\"Afghanistan\", " +
                "\"organizationType\":\"SHIPPING LINE\",\"address\":\"Milan\", \"organizationPhoneNumber\":\"8A9112497101\"," +
                "\"website\":\"www.abba.com\",\"organizationComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateOrganization?orgId=1")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callUpdateContactWithValidArgumentsThenIsOk() throws Exception {
        organization.setId(1);
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        contactRepository.save(contact);
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        organizationRepository.save(organization);

        String requestBody = "{\"contactName\":\"Monica\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101 (516)\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateContact?contactId=2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void callUpdateContactWithNullNameThenIs4xxClientError() throws Exception {
        organization.setId(1);
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        contactRepository.save(contact);
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        organizationRepository.save(organization);

        String requestBody = "{\"contactName\":\"\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101 (516)\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateContact?contactId=2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callUpdateContactWithIncorrectOfficePhoneNumberFormat4xxClientError() throws Exception {
        organization.setId(1);
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        contactRepository.save(contact);
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        organizationRepository.save(organization);

        String requestBody = "{\"contactName\":\"Monica\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101A (516)\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateContact?contactId=2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callUpdateContactWithIncorrectMobilePhoneNumberFormat4xxClientError() throws Exception {
        organization.setId(1);
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        contactRepository.save(contact);
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        organizationRepository.save(organization);

        String requestBody = "{\"contactName\":\"Monica\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101 (516)\",\"contactMobilePhoneNumber\":\"891124A97101\"," +
                "\"contactEmail\":\"w@com\",\"contactComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateContact?contactId=2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void callUpdateContactWithIncorrectEmailFormat4xxClientError() throws Exception {
        organization.setId(1);
        contact.setId(2);
        contact.setContactType("MANAGER");
        contact.setDirection("EXPORT");
        contactRepository.save(contact);
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        organization.setContacts(contactList);
        organizationRepository.save(organization);

        String requestBody = "{\"contactName\":\"Monica\",\"contactType\":\"MANAGER\",\"direction\":\"IMPORT AND EXPORT\"," +
                "\"contactOfficePhoneNumber\":\"89112497101 (516)\",\"contactMobilePhoneNumber\":\"89112497101\"," +
                "\"contactEmail\":\"wcom\",\"contactComments\":\"Air\"}";

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateContact?contactId=2")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en")
                .header("Authorization", "Bearer token"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }
}
