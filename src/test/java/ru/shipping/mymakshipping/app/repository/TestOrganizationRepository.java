package ru.shipping.mymakshipping.app.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.contact.ContactType;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.entities.organization.OrganizationType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:applicationTest.properties")
@Transactional
public class TestOrganizationRepository {
    @Autowired
    OrganizationRepository organizationRepository;

    @Autowired
    ContactRepository contactRepository;

    @Before
    public void configure() {
        Organization organization = new Organization();
        organization.setName("JAS");
        organization.setOrganizationType(OrganizationType.FORWARDER.getType());
        organization.setCountry("Germany");
        organization.setWebsite("www.jas.com");
        organization.setPhoneNumber("+792111111");
        organization.setAddress("Dresden");

        Organization organization2 = new Organization();
        organization2.setName("JASBO");
        organization2.setOrganizationType(OrganizationType.FORWARDER.getType());
        organization2.setCountry("Germany");
        organization2.setWebsite("www.jasbo.com");
        organization2.setPhoneNumber("+792511111");
        organization2.setAddress("Dortmund");

        Contact contact = new Contact();
        contact.setDirection("EXPORT");
        contact.setContactType(ContactType.SALES.toString());
        contact.setName("Lion");
        contact.setEmail("l@gm.com");
        contact.setOfficePhoneNumber("+7911213111");
        contact.setMobilePhoneNumber("+78124853111");
        contact.setOrganization(organization);

        Contact contact2 = new Contact();
        contact2.setDirection("IMPORT");
        contact2.setContactType(ContactType.SALES.toString());
        contact2.setName("Liora");
        contact2.setEmail("liora@gm.com");
        contact2.setOfficePhoneNumber("+791121311");
        contact2.setMobilePhoneNumber("+7812485311");
        contact2.setOrganization(organization);

        List<Contact> contacts = new ArrayList<>();
        contacts.add(contact);
        contacts.add(contact2);

        organization.setContacts(contacts);

        contactRepository.save(contact);
        contactRepository.save(contact2);
        organizationRepository.save(organization);
        organizationRepository.save(organization2);
    }

    @Test
    public void findOrganizationByIdThenSuccess() {
        long id = organizationRepository.findOrganizationByName("JAS").getId();
        Organization organization = organizationRepository.findOrganizationById(id);
        assertNotNull(organization);
        assertEquals(organization.getOrganizationType(), OrganizationType.FORWARDER.getType());
    }

    @Test
    public void findOrganizationByNameThenSuccessResult() {
        Organization organization = organizationRepository.findOrganizationByName("JAS");
        assertEquals(organization.getName(), "JAS");
        assertEquals(organization.getOrganizationType(), OrganizationType.FORWARDER.getType());
    }

    @Test
    public void findOrganizationByTypeThenSuccessResult() {
        List<Organization> organizations = organizationRepository.findByOrganizationType(OrganizationType.FORWARDER.getType());
        organizations.forEach(organization ->
                assertEquals(organization.getOrganizationType(), OrganizationType.FORWARDER.getType()));
    }


}
