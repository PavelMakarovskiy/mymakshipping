package ru.shipping.mymakshipping.app.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.shipping.mymakshipping.app.entities.role.ERole;
import ru.shipping.mymakshipping.app.entities.role.Role;
import ru.shipping.mymakshipping.app.entities.user.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:applicationTest.properties")
@Transactional
public class TestUserRepository {

    @Autowired
    private UserRepository userRepository;

    private Date now = new Date();

    @Before
    public void configure() {
        User newUser = new User();
        newUser.setUsername("Rita");
        newUser.setPassword("1234");
        newUser.setEmail("r@inbox.com");
        List<Role> roles = new ArrayList<>();
        Role userRole = new Role();
        userRole.setRoleName(ERole.ROLE_USER.toString());
        newUser.setRoles(roles);
        newUser.setCreated(new Date());

        User newUser2 = new User();
        newUser2.setUsername("Rota");
        newUser2.setPassword("12345");
        newUser2.setEmail("rota@inbox.com");
        List<Role> roles2 = new ArrayList<>();
        Role userRole2 = new Role();
        userRole2.setRoleName(ERole.ROLE_USER.toString());
        newUser.setRoles(roles2);
        newUser.setCreated(new Date());

        userRepository.save(newUser);
        userRepository.save(newUser2);
    }

    @Test
    public void findByUsername() {
        assertNull(userRepository.findUserByUsername("not such user"));
        User found = userRepository.findUserByUsername("Rita");
        assertNotNull(found);
        assertEquals("Rita", found.getUsername());
    }

    @Test
    public void findByEmail() {
        assertNull(userRepository.findUserByEmail("wrong mail"));
        User found = userRepository.findUserByEmail("r@inbox.com");
        assertNotNull(found);
        assertEquals("r@inbox.com", found.getEmail());
    }

}
