package ru.shipping.mymakshipping.app.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.contact.ContactType;
import ru.shipping.mymakshipping.app.entities.organization.Organization;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(
        locations = "classpath:applicationTest.properties")
@Transactional
public class TestContactRepository {

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    OrganizationRepository organizationRepository;

    @Before
    public void configure() {
        Organization organization = new Organization();
        organization.setName("MSC");
        organization.setCountry("France");
        organization.setOrganizationType("SHIPPING LINE");
        Contact contact = new Contact();
        contact.setName("Zosia");
        contact.setContactType(ContactType.SALES.toString());
        contact.setDirection("EXPORT");
        contact.setEmail("w@gm.com");
        contact.setOfficePhoneNumber("+7911213");
        contact.setMobilePhoneNumber("+78124853");
        contact.setComments("the best sales");
        contact.setOrganization(organization);

        Contact contact2 = new Contact();
        contact2.setName("Zosia2");
        contact2.setContactType(ContactType.SALES.toString());
        contact2.setDirection("IMPORT");
        contact2.setEmail("z2@gm.com");
        contact2.setOfficePhoneNumber("+7911213");
        contact2.setMobilePhoneNumber("+78124853");
        contact2.setComments("sales #2");
        contact2.setOrganization(organization);

        organizationRepository.save(organization);
        contactRepository.save(contact);
        contactRepository.save(contact2);
    }

    @Test
    public void findOrganizationByIdThenSuccess() {
        long id = contactRepository.findContactByName("Zosia").getId();
        Contact contact = contactRepository.findContactById(id);
        assertNotNull(contact);
        assertEquals(contact.getContactType(), ContactType.SALES.toString());
    }

    @Test
    public void findContactByNameThenSuccess() {
        Contact testContact = contactRepository.findContactByName("Zosia");
        assertNotNull(testContact);
        assertEquals(testContact.getContactType(), ContactType.SALES.toString());
    }

    @Test
    public void findContactByTypeThenSuccessResult() {
        List<Contact> contacts = contactRepository.findContactsByContactType(ContactType.SALES.name());
        contacts.forEach(contact ->
                assertEquals(contact.getContactType(), ContactType.SALES.name()));
    }

    @Test
    public void findAllContactsThenSuccess() {
        List<Contact> contacts = (List<Contact>) contactRepository.findAll();
        assertNotNull(contacts.get(0));
        assertNotNull(contacts.get(1));
        assertEquals(contacts.size(), 2);
    }
}
