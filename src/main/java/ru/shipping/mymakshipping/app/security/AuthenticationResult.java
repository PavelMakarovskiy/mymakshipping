package ru.shipping.mymakshipping.app.security;

import lombok.Data;

@Data
public class AuthenticationResult {
    private String username;

    private String token;
}
