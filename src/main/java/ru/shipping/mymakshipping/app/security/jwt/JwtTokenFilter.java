package ru.shipping.mymakshipping.app.security.jwt;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import ru.shipping.mymakshipping.app.repository.JwtBlackListRepository;
import ru.shipping.mymakshipping.app.security.JwtBlackList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class JwtTokenFilter extends GenericFilterBean {
    private JwtTokenProvider jwtTokenProvider;

    private JwtBlackListRepository jwtBlackListRepository;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider,
                          JwtBlackListRepository jwtBlackListRepository) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtBlackListRepository = jwtBlackListRepository;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String token = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        JwtBlackList jwtBlackList = jwtBlackListRepository.findJwtBlackListByBlackToken(token);
        try {
            if (token != null && jwtTokenProvider.validateToken(token) && jwtBlackList == null) {
                Authentication auth = jwtTokenProvider.getAuthentication(token);
                if (auth != null) {
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            }
        } catch (AuthenticationException authenticationException) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        chain.doFilter(request, response);
    }
}
