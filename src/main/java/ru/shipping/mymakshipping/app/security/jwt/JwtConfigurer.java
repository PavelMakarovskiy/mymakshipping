package ru.shipping.mymakshipping.app.security.jwt;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.shipping.mymakshipping.app.repository.JwtBlackListRepository;

public class JwtConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private JwtTokenProvider jwtTokenProvider;

    private JwtBlackListRepository jwtBlackListRepository;

    public JwtConfigurer(JwtTokenProvider jwtTokenProvider,
                         JwtBlackListRepository jwtBlackListRepository) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtBlackListRepository = jwtBlackListRepository;
    }

    @Override
    public void configure(HttpSecurity httpSecurity) {
        JwtTokenFilter jwtTokenFilter = new JwtTokenFilter(jwtTokenProvider, jwtBlackListRepository);
        httpSecurity.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
