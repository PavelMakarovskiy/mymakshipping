package ru.shipping.mymakshipping.app.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.shipping.mymakshipping.app.repository.UserRepository;
import ru.shipping.mymakshipping.app.security.jwt.JwtUserFactory;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public JwtUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ru.shipping.mymakshipping.app.entities.user.User found = userRepository.findUserByUsername(username);
        if (found == null) {
            throw new UsernameNotFoundException("User " + username + " not found");
        }
        return JwtUserFactory.create(found);
    }
}
