package ru.shipping.mymakshipping.app.security;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "jwt_black_list")
@Entity
public class JwtBlackList {
    @Column(name = "black_tokens")
    @Id
    private String blackToken;
}
