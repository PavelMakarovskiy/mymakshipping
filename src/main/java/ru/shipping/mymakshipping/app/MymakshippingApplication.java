package ru.shipping.mymakshipping.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MymakshippingApplication {

    public static void main(String[] args) {

        SpringApplication.run(MymakshippingApplication.class, args);
    }
}
