package ru.shipping.mymakshipping.app.jsonDeserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.shipping.mymakshipping.app.entities.role.Role;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RolesJsonDeserializer extends JsonDeserializer<Map<Integer, Role>> {
    @Override
    public Map<Integer, Role> deserialize(JsonParser jsonParser, DeserializationContext ctxt) {
        Map<Integer, Role> mapRoles = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapRoles = mapper.readValue(jsonParser, new TypeReference<HashMap<Integer, Role>>() {
            });
        } catch (IOException ex) {
            ex.getMessage();
        }
        return mapRoles;
    }
}


