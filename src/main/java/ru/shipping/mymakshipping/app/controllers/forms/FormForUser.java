package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class FormForUser {
    @NotBlank(message = "username is mandatory")
    @Length(max = 100)
    private String username;

    @Email(message = "input valid email address")
    private String email;

    @NotBlank(message = "username is mandatory")
    private String password;

}
