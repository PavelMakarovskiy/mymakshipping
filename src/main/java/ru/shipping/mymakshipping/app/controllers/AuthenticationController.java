package ru.shipping.mymakshipping.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.shipping.mymakshipping.app.controllers.forms.LoginForm;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.UserRepository;
import ru.shipping.mymakshipping.app.security.AuthenticationResult;
import ru.shipping.mymakshipping.app.security.jwt.JwtTokenProvider;

import javax.validation.Valid;
import java.util.Locale;

@RestController
@CrossOrigin(origins = "http://localhost")
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserRepository userRepository;

    private final MessageSource messageSource;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager,
                                    JwtTokenProvider jwtTokenProvider, UserRepository userRepository, MessageSource messageSource) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userRepository = userRepository;
        this.messageSource = messageSource;
    }

    @PostMapping("/loginPage")
    public AuthenticationResult login(@Valid @RequestBody LoginForm loginForm, Locale locale) {
        try {
            String username = loginForm.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginForm.getPassword()));
            User user = userRepository.findUserByUsername(username);

            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found.");
            }

            String token = jwtTokenProvider.createToken(username, user.getRoles());

            AuthenticationResult authenticationResult = new AuthenticationResult();
            authenticationResult.setUsername(username);
            authenticationResult.setToken(token);

            return authenticationResult;
        } catch (AuthenticationException e) {
            String invalidUsernameOrPasswordMessage = messageSource.getMessage(
                    "authenticationController.invalidUsernameOrPassword", new Object[]{}, locale);
            throw new BadCredentialsException(invalidUsernameOrPasswordMessage);
        }
    }

    @PostMapping("/logOut")
    public AuthenticationResult logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        AuthenticationResult authenticationResult = new AuthenticationResult();
        authenticationResult.setToken(null);
        return authenticationResult;
    }
}
