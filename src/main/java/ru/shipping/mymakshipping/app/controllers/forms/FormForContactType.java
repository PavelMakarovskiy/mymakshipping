package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;
import ru.shipping.mymakshipping.app.entities.contact.ContactType;

@Data
public class FormForContactType {
    private ContactType contactType;
}
