package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import ru.shipping.mymakshipping.app.entities.contact.Contact;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Data
public class FormForOrganization {
    @NotBlank
    @Length(max = 50)
    private String organizationName;

    @NotBlank
    private String organizationCountry;

    @NotBlank
    private String organizationType;

    private String website;

    private String address;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s]){1}$)")
    private String organizationPhoneNumber;

    @Length(max = 1024)
    private String organizationComments;

    private List<Contact> contacts = new ArrayList<>();

}
