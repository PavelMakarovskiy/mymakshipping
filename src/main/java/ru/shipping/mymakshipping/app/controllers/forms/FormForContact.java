package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class FormForContact {
    @NotBlank
    @Length(max = 50)
    private String contactName;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s)]){1}$)")
    private String contactOfficePhoneNumber;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s]){1}$)")
    private String contactMobilePhoneNumber;

    @Email
    private String contactEmail;

    @Length(max = 1024)
    private String contactComments;

    @NotBlank
    private String contactType;

    @NotBlank
    private String direction;

    private long organizationId;
}
