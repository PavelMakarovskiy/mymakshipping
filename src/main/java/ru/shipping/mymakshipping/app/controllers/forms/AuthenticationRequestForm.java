package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;

@Data
public class AuthenticationRequestForm {
    private String username;

    private String password;
}
