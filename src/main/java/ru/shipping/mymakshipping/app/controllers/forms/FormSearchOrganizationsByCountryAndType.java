package ru.shipping.mymakshipping.app.controllers.forms;

import lombok.Data;

@Data
public class FormSearchOrganizationsByCountryAndType {
    private String organizationCountry;

    private String organizationType;
}
