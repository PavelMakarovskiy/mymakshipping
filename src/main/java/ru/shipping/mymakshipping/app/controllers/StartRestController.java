package ru.shipping.mymakshipping.app.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.shipping.mymakshipping.app.controllers.forms.RegistrationForm;
import ru.shipping.mymakshipping.app.entities.additional.Country;
import ru.shipping.mymakshipping.app.entities.contact.ContactType;
import ru.shipping.mymakshipping.app.entities.contact.Direction;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.entities.organization.OrganizationType;
import ru.shipping.mymakshipping.app.entities.role.ERole;
import ru.shipping.mymakshipping.app.entities.role.Role;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;
import ru.shipping.mymakshipping.app.repository.RoleRepository;
import ru.shipping.mymakshipping.app.repository.UserRepository;
import ru.shipping.mymakshipping.app.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost")
@RestController
public class StartRestController {
    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private OrganizationRepository organizationRepository;

    private IUserService userService;

    public StartRestController(UserRepository userRepository, RoleRepository roleRepository,
                               OrganizationRepository organizationRepository, IUserService userService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.organizationRepository = organizationRepository;
        this.userService = userService;
    }

    @PostMapping("/registrationPage")
    public ResponseEntity<User> register(@Valid @RequestBody RegistrationForm registrationForm, Locale locale, HttpServletRequest request) throws NoSuchMethodException, MethodArgumentNotValidException, MalformedURLException {
        URL url = new URL(request.getRequestURL().toString());
        String host = url.getHost();
        return userService.saveUser(registrationForm, locale, host);
    }

    @GetMapping("/authStatus")
    public String getAuthStatus(HttpServletRequest request) {
        return userService.authStatus(request);
    }

    @GetMapping("/organizationTypeList")
    public List<String> getOrganizationTypeList() {
        List<String> types = Arrays.stream(OrganizationType.values()).map(OrganizationType::getType).sorted().collect(Collectors.toList());
        return types;
    }

    @GetMapping("/contactTypeList")
    public List<ContactType> getContactTypeList() {
        return Arrays.asList(ContactType.values());
    }

    @GetMapping("/countryListFromExistingOrganizations")
    public List<String> getCountriesFromExistingOrganizations() {
        List<Organization> organizationList = organizationRepository.findAll();
        return organizationList.stream()
                .map(organization -> organization.getCountry())
                .collect(Collectors.toSet()).stream().sorted().collect(Collectors.toList());
    }

    @GetMapping("/countryList")
    public List<String> getCountries() {
        return Country.getCountries();
    }

    @GetMapping("/getDirectionList")
    public List<String> getDirections() {
        return Arrays.stream(Direction.values()).map(Direction::getDirection).collect(Collectors.toList());
    }

    @PostMapping("/adminRequest")
    public boolean isAdmin(@RequestParam(name = "adminName") String adminName) {
        if (userRepository.findUserByUsername(adminName) != null) {
            return userRepository.findUserByUsername(adminName).getRoles()
                    .stream().anyMatch(role -> role.getRoleName().equals(ERole.ROLE_ADMIN.name()));
        }
        return false;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/adminFunctions")
    public ResponseEntity<List<User>> getUserList() {
        return userService.getUserLists();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/rolesList")
    public ResponseEntity<List<Role>> getRolesList() {
        return new ResponseEntity<>(roleRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/activate/{code}")
    public String activate(@PathVariable String code, HttpServletRequest request) throws MalformedURLException {
        URL url = new URL(request.getRequestURL().toString());
        String host = url.getHost();
        return userService.activateUser(code, host);
    }

}
