package ru.shipping.mymakshipping.app.controllers.forms;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import ru.shipping.mymakshipping.app.entities.role.Role;
import ru.shipping.mymakshipping.app.jsonDeserializer.RolesJsonDeserializer;

import java.util.Map;

@Data
public class UserUpdateForm {
    private long id;

    private String username;

    private String email;

    @JsonDeserialize(using = RolesJsonDeserializer.class)
    private Map<Integer, Role> roles;

    private String isActive;
}
