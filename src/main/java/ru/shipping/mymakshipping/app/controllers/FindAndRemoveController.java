package ru.shipping.mymakshipping.app.controllers;

import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ru.shipping.mymakshipping.app.controllers.forms.FormForContactType;
import ru.shipping.mymakshipping.app.controllers.forms.FormSearchOrganizationsByCountryAndType;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;
import ru.shipping.mymakshipping.app.service.IContactService;
import ru.shipping.mymakshipping.app.service.IOrganizationService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost")
@RestController
public class FindAndRemoveController {
    private OrganizationRepository organizationRepository;

    private ContactRepository contactRepository;

    private IContactService contactService;

    private IOrganizationService organizationService;

    public FindAndRemoveController(OrganizationRepository organizationRepository, ContactRepository contactRepository,
                                   IContactService contactService, IOrganizationService organizationService) {
        this.organizationRepository = organizationRepository;
        this.contactRepository = contactRepository;
        this.contactService = contactService;
        this.organizationService = organizationService;
    }

    @GetMapping("/getOrganization")
    public Organization getOrganization(@RequestParam(name = "orgId") long orgId) throws NotFoundException {
        Organization org = organizationRepository.findOrganizationById(orgId);
        if (org != null) {
            return org;
        } else {
            throw new NotFoundException("No organization with such id");
        }
    }

    @GetMapping("/getContacts")
    public List<Contact> getContacts(@RequestParam(name = "orgId") long orgId) throws NotFoundException {
        Organization org = organizationRepository.findOrganizationById(orgId);
        if (org != null) {
            return contactService.findPaginatedAllContactsByOrgId(orgId);
        } else {
            throw new NotFoundException("No organization with such id");
        }
    }

    @GetMapping("/getContact")
    public Contact getContactById(@RequestParam(name = "contactId") long contactId) throws NotFoundException {
        Contact contact = contactRepository.findContactById(contactId);
        if (contact != null) {
            return contact;
        } else {
            throw new NotFoundException("No contact with such id");
        }
    }

    @PostMapping("/searchOrganizations")
    public Page<Organization> getOrganizationsByCountryAndType(
            @RequestBody FormSearchOrganizationsByCountryAndType form,
            @RequestParam(name = "pageNo") int pageNo,
            @RequestParam(name = "pageSize") int pageSize) {
        return organizationService.getOrganizationsByCountryAndType(form, pageNo, pageSize);
    }

    @PostMapping("/seeContactsDetailsByPosition")
    public List<Contact> seeContactsDetailsByPosition(@RequestBody FormForContactType formForContactType,
                                                      @RequestParam(name = "orgId") long orgId) {
        List<Contact> contacts = contactRepository.findContactsByOrganizationId(orgId).stream().
                filter(contact -> contact.getContactType().equals(formForContactType.getContactType().toString()))
                .sorted(Comparator.comparing(Contact::getName)).collect(Collectors.toList());
        return contacts;
    }

    @PostMapping("/getAllOrganizations")
    public List<Organization> getAllOrganizations() {
        return organizationService.getAllOrganizations();
    }

    @PostMapping(value = "/removeOrganization")
    public boolean removeOrganization(@RequestBody long orgId) {
        organizationRepository.deleteById(orgId);
        return organizationRepository.findOrganizationById(orgId) == null;
    }

    @PostMapping(value = "/removeContact")
    public boolean removeContact(@RequestBody long contactId) {
        return contactService.removeContact(contactId);
    }
}
