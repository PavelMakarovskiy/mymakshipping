package ru.shipping.mymakshipping.app.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.shipping.mymakshipping.app.controllers.forms.FormForContact;
import ru.shipping.mymakshipping.app.controllers.forms.FormForOrganization;
import ru.shipping.mymakshipping.app.controllers.forms.UserUpdateForm;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;
import ru.shipping.mymakshipping.app.service.IContactService;
import ru.shipping.mymakshipping.app.service.IOrganizationService;
import ru.shipping.mymakshipping.app.service.IUserService;

import javax.validation.Valid;
import java.util.Locale;

@RestController
public class AddAndUpdateController {
    private OrganizationRepository organizationRepository;

    private ContactRepository contactRepository;

    private IOrganizationService organizationService;

    private IContactService contactService;

    private IUserService userService;

    public AddAndUpdateController(OrganizationRepository organizationRepository, ContactRepository contactRepository,
                                  IOrganizationService organizationService, IContactService contactService, IUserService userService) {
        this.organizationRepository = organizationRepository;
        this.contactRepository = contactRepository;
        this.organizationService = organizationService;
        this.contactService = contactService;
        this.userService = userService;
    }

    @PostMapping("/addOrganization")
    public ResponseEntity<Organization> addOrganization(
            @Valid @RequestBody FormForOrganization formForOrganization, Locale locale) throws MethodArgumentNotValidException, NoSuchMethodException {
        return (ResponseEntity<Organization>) organizationService.saveOrganization(formForOrganization, locale);
    }

    @PostMapping("/addContact")
    public ResponseEntity<Contact> addContact(@Valid @RequestBody FormForContact formForContact) {
        return contactService.saveContact(formForContact);
    }

    @PostMapping("/updateOrganization")
    public ResponseEntity<Organization> updateOrganization(@Valid @RequestBody FormForOrganization formForOrganization,
                                                           @RequestParam(name = "orgId") long orgId, Locale locale) throws NoSuchMethodException, MethodArgumentNotValidException {
        return (ResponseEntity<Organization>) organizationService.updateOrganization(orgId, formForOrganization, locale);
    }

    @PostMapping("/updateContact")
    public ResponseEntity<Contact> updateContact(@Valid @RequestBody FormForContact formForContact,
                                                 @RequestParam(name = "contactId") long contactId) {
        return contactService.updateContact(contactId, formForContact);
    }

    @PostMapping("/updateUserDetails")
    public ResponseEntity<User> updateUserDetails(@Valid @RequestBody UserUpdateForm userUpdateForm) {
        return userService.updateUserDetails(userUpdateForm);
    }
}


