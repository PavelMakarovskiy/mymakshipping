package ru.shipping.mymakshipping.app.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.shipping.mymakshipping.app.controllers.forms.RegistrationForm;
import ru.shipping.mymakshipping.app.controllers.forms.UserUpdateForm;
import ru.shipping.mymakshipping.app.entities.user.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@Service
public interface IUserService {
    ResponseEntity<User> saveUser(RegistrationForm registrationForm, Locale locale, String host) throws NoSuchMethodException, MethodArgumentNotValidException;

    ResponseEntity<List<User>> getUserLists();

    ResponseEntity<User> updateUserDetails(UserUpdateForm userUpdateForm);

    String activateUser(String code, String host);

    String authStatus(HttpServletRequest request);
}
