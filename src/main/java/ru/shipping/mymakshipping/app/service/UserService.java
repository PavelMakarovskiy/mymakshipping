package ru.shipping.mymakshipping.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.shipping.mymakshipping.app.controllers.forms.RegistrationForm;
import ru.shipping.mymakshipping.app.controllers.forms.UserUpdateForm;
import ru.shipping.mymakshipping.app.entities.role.ERole;
import ru.shipping.mymakshipping.app.entities.role.Role;
import ru.shipping.mymakshipping.app.entities.user.User;
import ru.shipping.mymakshipping.app.repository.JwtBlackListRepository;
import ru.shipping.mymakshipping.app.repository.RoleRepository;
import ru.shipping.mymakshipping.app.repository.UserRepository;
import ru.shipping.mymakshipping.app.security.IAuthenticationFacade;
import ru.shipping.mymakshipping.app.security.JwtBlackList;
import ru.shipping.mymakshipping.app.security.jwt.JwtTokenProvider;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    private MessageSource messageSource;

    private MailSender mailSender;

    private IAuthenticationFacade authenticationFacade;

    private JwtTokenProvider jwtTokenProvider;

    private JwtBlackListRepository jwtBlackListRepository;

    @Value("${spring.mail.notificationForAdmin.email}")
    private String notificationForAdmin;

    public UserService(UserRepository userRepository, RoleRepository roleRepository,
                       PasswordEncoder passwordEncoder, MessageSource messageSource,
                       MailSender mailSender) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.messageSource = messageSource;
        this.mailSender = mailSender;
    }

    @Autowired
    public void setAuthenticationFacade(IAuthenticationFacade authenticationFacade) {
        this.authenticationFacade = authenticationFacade;
    }

    @Autowired
    public void setJwtBlackListRepository(JwtBlackListRepository jwtBlackListRepository) {
        this.jwtBlackListRepository = jwtBlackListRepository;
    }

    @Autowired
    public void setJwtTokenProvider(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public ResponseEntity<User> saveUser(RegistrationForm registrationForm, Locale locale, String host) throws NoSuchMethodException, MethodArgumentNotValidException {
        checkValidation(registrationForm, locale);
        User user = new User();
        user.setUsername(registrationForm.getUsername());
        user.setEmail(registrationForm.getEmail());
        user.setPassword(passwordEncoder.encode(registrationForm.getPassword()));
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findRoleByRoleName(ERole.ROLE_USER.toString()));
        user.setRoles(roles);
        user.setCreated(new Date());
        user.setActivationCode(UUID.randomUUID().toString());
        user.setActive(false);
        User savedUser = userRepository.save(user);

        if (!user.getEmail().isEmpty()) {
            sendActivationInfo(user, host);
        }
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    public void sendActivationInfo(User user, String host) {
        String messageToUser = String.format(
                "Hello %s! Welcome to MakShipping." +
                        " Please visit link for activation in MakShipping: " + "http://" + host + "/api/activate/%s",
                user.getUsername(),
                user.getActivationCode()
        );
        String messageToAdmin = String.format("User " + user.getUsername() + " with id " + user.getId() + " requested activation in MakShipping");
        mailSender.send(user.getEmail(), "Activation code for MakShipping", messageToUser);
        mailSender.send(notificationForAdmin, "Notification about activation in MakShipping.", messageToAdmin);
    }

    public void checkValidation(RegistrationForm registrationForm, Locale locale) throws MethodArgumentNotValidException, NoSuchMethodException {
        String fieldName = "username";
        String message = messageSource.getMessage(
                "userService.usernameExist", new Object[]{}, locale);
        String fieldName2 = "email";
        String message2 = messageSource.getMessage(
                "userService.userEmailExist", new Object[]{}, locale);
        MethodParameter methodParameter = new MethodParameter(this.getClass().getMethod("saveUser", RegistrationForm.class, Locale.class, String.class), 0);
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(registrationForm, "registrationForm");
        FieldError fieldError = new FieldError("registrationForm", fieldName, message);
        FieldError fieldError2 = new FieldError("registrationForm", fieldName2, message2);
        if (userRepository.findUserByUsername(registrationForm.getUsername()) != null &&
                userRepository.findUserByEmail(registrationForm.getEmail()) != null) {
            result.addError(fieldError);
            result.addError(fieldError2);
            throw new MethodArgumentNotValidException(methodParameter, result);
        } else if (userRepository.findUserByUsername(registrationForm.getUsername()) != null) {
            result.addError(fieldError);
            throw new MethodArgumentNotValidException(methodParameter, result);
        } else if (userRepository.findUserByEmail(registrationForm.getEmail()) != null) {
            result.addError(fieldError2);
            throw new MethodArgumentNotValidException(methodParameter, result);
        }
    }

    @Override
    public ResponseEntity<List<User>> getUserLists() {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<User> updateUserDetails(UserUpdateForm userUpdateForm) {
        User currentUser = userRepository.findUserById(userUpdateForm.getId());
        currentUser.setUsername(userUpdateForm.getUsername());
        currentUser.setEmail(userUpdateForm.getEmail());
        List<Long> updateRoleIds = new ArrayList<>();
        for (Map.Entry<Integer, Role> nextEntry : userUpdateForm.getRoles().entrySet()) {
            long updateRoleId = nextEntry.getValue().getRoleId();
            String updateRoleName = nextEntry.getValue().getRoleName();
            if (updateRoleId != 0 && !updateRoleName.isBlank()) {
                updateRoleIds.add(updateRoleId);
                if (currentUser.getRoles().stream().noneMatch(role -> role.getRoleId() == updateRoleId)) {
                    currentUser.getRoles().add(roleRepository.findRoleByRoleId(updateRoleId));
                }
            }
        }
        List<Role> roleList = currentUser.getRoles().stream().filter(role -> updateRoleIds.contains(role.getRoleId()))
                .collect((Collectors.toList()));
        currentUser.setRoles(roleList);
        currentUser.setActive(Boolean.parseBoolean(userUpdateForm.getIsActive()));
        return new ResponseEntity<>(userRepository.save(currentUser), HttpStatus.CREATED);
    }

    @Override
    public String activateUser(String code, String host) {
        if (userRepository.findByActivationCode(code) != null) {
            User user = userRepository.findByActivationCode(code);
            user.setActive(true);
            userRepository.save(user);
            return user.getUsername() + " is successfully activated. Welcome to MakShipping app: http://" + host + "/index.html";
        } else {
            return "User is not activated. \n" +
                    "Please check activation code/link which were sent to your email, \n" +
                    "ask for a help from administrator or you may create new account. \n" +
                    " Home page: http://" + host + "/index.html";
        }
    }

    @Override
    public String authStatus(HttpServletRequest request) {
        String result = "anonymous";
        Authentication auth = authenticationFacade.getAuthentication();
        if ((!(auth instanceof AnonymousAuthenticationToken)) && auth != null) {
            UserDetails principal = (UserDetails) auth.getPrincipal();
            User currentUser = userRepository.findUserByUsername(principal.getUsername());
            boolean isUserActivated = currentUser.isActive();
            if (isUserActivated) {
                result = principal.getUsername();
            } else {
                String token = jwtTokenProvider.resolveToken(request);
                JwtBlackList jwtBlackList = new JwtBlackList();
                jwtBlackList.setBlackToken(token);
                jwtBlackListRepository.save(jwtBlackList);
            }
        } else {
            result = "anonymous";
        }
        return result;
    }
}
