package ru.shipping.mymakshipping.app.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.shipping.mymakshipping.app.controllers.forms.FormForContact;
import ru.shipping.mymakshipping.app.entities.contact.Contact;

import java.util.List;

@Service
public interface IContactService {
    List<Contact> findPaginatedAllContactsByOrgId(long id);

    ResponseEntity<Contact> saveContact(FormForContact formForContact);

    ResponseEntity<Contact> updateContact(long id, FormForContact formForContact);

    boolean removeContact(long contactId);
}
