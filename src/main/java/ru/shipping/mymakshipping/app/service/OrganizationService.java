package ru.shipping.mymakshipping.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.shipping.mymakshipping.app.controllers.forms.FormForOrganization;
import ru.shipping.mymakshipping.app.controllers.forms.FormSearchOrganizationsByCountryAndType;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.exception.RestExceptionHandler;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;

@Service
@Validated
public class OrganizationService implements IOrganizationService {

    private ContactRepository contactRepository;

    private OrganizationRepository organizationRepository;

    private RestExceptionHandler exceptionHandler;

    private MessageSource messageSource;

    @Autowired
    public void setContactRepository(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Autowired
    public void setOrganizationRepository(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Autowired
    public void setExceptionHandler(RestExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public Page<Organization> getOrganizationsByCountryAndType(FormSearchOrganizationsByCountryAndType form,
                                                               int pageNo,
                                                               int pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("name"));
        Page<Organization> organizationByCountryAndByType = null;
        Page<Organization> organizationsByCountry;
        String country = form.getOrganizationCountry();
        String type = form.getOrganizationType();
        if (!country.equals("Any country") && !type.equals("Any type")) {
            organizationByCountryAndByType = organizationRepository.findOrganizationByCountryAndOrganizationType(
                    country, type, paging);
        } else {
            if (country.equals("Any country")) {
                organizationsByCountry = organizationRepository.findAll(paging);
            } else {
                organizationsByCountry = organizationRepository.findOrganizationByCountry(country, paging);
            }
            if (type.equals("Any type")) {
                organizationByCountryAndByType = organizationsByCountry;
            } else {
                organizationByCountryAndByType = organizationRepository.findByOrganizationType(type, paging);
            }
        }
        return organizationByCountryAndByType;
    }

    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> organizations = organizationRepository.findAll();
        organizations.sort(Comparator.comparing(Organization::getName));
        return organizations;
    }

    @Override
    public ResponseEntity<? extends Object> saveOrganization(FormForOrganization formForOrganization, Locale locale) throws MethodArgumentNotValidException, NoSuchMethodException {
        if (organizationRepository.findOrganizationByName(formForOrganization.getOrganizationName()) != null) {
            MethodParameter methodParameter = new MethodParameter(this.getClass().getMethod("saveOrganization", FormForOrganization.class, Locale.class), 0);
            return throwMethodArgumentNotValidException(methodParameter, formForOrganization, locale);
        } else {
            Organization organization = new Organization();
            Organization savedOrganization = organizationRepository.save(setUpOrganization(organization, formForOrganization));
            return new ResponseEntity<>(savedOrganization, HttpStatus.CREATED);
        }
    }

    @Override
    public ResponseEntity<? extends Object> updateOrganization(long orgId, FormForOrganization formForOrganization, Locale locale) throws NoSuchMethodException, MethodArgumentNotValidException {
        Organization organizationById = organizationRepository.findOrganizationById(orgId);
        Organization organizationByName = organizationRepository.findOrganizationByName(formForOrganization.getOrganizationName());
        if (organizationByName != null && organizationById.getId() != organizationByName.getId()) {
            MethodParameter methodParameter = new MethodParameter(this.getClass().getMethod("updateOrganization", long.class, FormForOrganization.class, Locale.class), 0);
            return throwMethodArgumentNotValidException(methodParameter, formForOrganization, locale);
        } else {
            Organization savedOrganization = organizationRepository.save(
                    setUpOrganization(organizationById, formForOrganization));
            return new ResponseEntity<>(savedOrganization, HttpStatus.ACCEPTED);
        }
    }

    public Organization setUpOrganization(Organization organization, FormForOrganization formForOrganization) {
        organization.setName(formForOrganization.getOrganizationName());
        organization.setOrganizationType(formForOrganization.getOrganizationType());
        organization.setCountry(formForOrganization.getOrganizationCountry());
        organization.setPhoneNumber(formForOrganization.getOrganizationPhoneNumber());
        organization.setAddress(formForOrganization.getAddress());
        organization.setWebsite(formForOrganization.getWebsite());
        organization.setComments(formForOrganization.getOrganizationComments());
        return organizationRepository.save(organization);
    }

    public ResponseEntity<? extends Object> throwMethodArgumentNotValidException(MethodParameter methodParameter, FormForOrganization formForOrganization, Locale locale) throws MethodArgumentNotValidException {
        String fieldName = "organizationName";
        String duplicateMessage = messageSource.getMessage(
                "organizationService.duplicateOrganization", new Object[]{}, locale);
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(formForOrganization, "formForOrganization");
        FieldError fieldError = new FieldError("formForOrganization", fieldName, duplicateMessage);
        result.addError(fieldError);
        throw new MethodArgumentNotValidException(methodParameter, result);
    }
}


