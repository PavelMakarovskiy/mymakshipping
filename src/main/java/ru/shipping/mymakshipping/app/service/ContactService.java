package ru.shipping.mymakshipping.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.shipping.mymakshipping.app.controllers.forms.FormForContact;
import ru.shipping.mymakshipping.app.entities.contact.Contact;
import ru.shipping.mymakshipping.app.entities.organization.Organization;
import ru.shipping.mymakshipping.app.repository.ContactRepository;
import ru.shipping.mymakshipping.app.repository.OrganizationRepository;

import java.util.Comparator;
import java.util.List;

@Service
public class ContactService implements IContactService {

    private ContactRepository contactRepository;

    private OrganizationRepository organizationRepository;

    @Autowired
    public void setContactRepository(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Autowired
    public void setOrganizationRepository(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Override
    public List<Contact> findPaginatedAllContactsByOrgId(long orgId) {
        List<Contact> contacts = contactRepository.findContactsByOrganizationId(orgId);
        contacts.sort(Comparator.comparing(Contact::getName));
        return contacts;
    }

    @Override
    public ResponseEntity<Contact> saveContact(FormForContact formForContact) {
        Contact contact = new Contact();
        Organization organization = organizationRepository.findOrganizationById(formForContact.getOrganizationId());
        contact.setOrganization(organization);
        return new ResponseEntity<>(contactRepository.save(setUpContact(contact, formForContact)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Contact> updateContact(long id, FormForContact formForContact) {
        Contact contact = contactRepository.findContactById(id);
        return new ResponseEntity<>(contactRepository.save(setUpContact(contact, formForContact)), HttpStatus.ACCEPTED);
    }

    @Override
    public boolean removeContact(long contactId) {
        Contact contact = contactRepository.findContactById(contactId);
        long orgId = contact.getOrganization().getId();
        Organization organization = organizationRepository.findOrganizationById(orgId);
        organization.getContacts().remove(contactRepository.findContactById(contactId));
        contactRepository.deleteById(contactId);
        return contactRepository.findContactById(contactId) == null &&
                !organization.getContacts().contains(contact);
    }

    public Contact setUpContact(Contact contact, FormForContact formForContact) {
        contact.setName(formForContact.getContactName());
        contact.setDirection(formForContact.getDirection());
        contact.setContactType(formForContact.getContactType());
        contact.setEmail(formForContact.getContactEmail());
        contact.setOfficePhoneNumber(formForContact.getContactOfficePhoneNumber());
        contact.setMobilePhoneNumber(formForContact.getContactMobilePhoneNumber());
        contact.setComments(formForContact.getContactComments());
        return contact;
    }
}
