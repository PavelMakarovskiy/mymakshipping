package ru.shipping.mymakshipping.app.service;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.shipping.mymakshipping.app.controllers.forms.FormForOrganization;
import ru.shipping.mymakshipping.app.controllers.forms.FormSearchOrganizationsByCountryAndType;
import ru.shipping.mymakshipping.app.entities.organization.Organization;

import java.util.List;
import java.util.Locale;

@Service
public interface IOrganizationService {

    Page<Organization> getOrganizationsByCountryAndType(FormSearchOrganizationsByCountryAndType form, int pageNo, int pageSize);

    List<Organization> getAllOrganizations();

    ResponseEntity<? extends Object> saveOrganization(FormForOrganization formForOrganization, Locale locale) throws MethodArgumentNotValidException, NoSuchMethodException;

    ResponseEntity<? extends Object> updateOrganization(long orgId, FormForOrganization formForOrganization, Locale locale) throws NoSuchMethodException, MethodArgumentNotValidException;
}
