package ru.shipping.mymakshipping.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.shipping.mymakshipping.app.entities.user.User;

@Repository
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserById(long id);

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User findByActivationCode(String code);
}
