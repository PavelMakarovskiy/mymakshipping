package ru.shipping.mymakshipping.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shipping.mymakshipping.app.security.JwtBlackList;

@Repository
public interface JwtBlackListRepository extends JpaRepository<JwtBlackList, String> {
    JwtBlackList findJwtBlackListByBlackToken(String token);
}
