package ru.shipping.mymakshipping.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shipping.mymakshipping.app.entities.organization.Organization;

import java.util.List;

@Repository
public interface OrganizationRepository extends CrudRepository<Organization, Long> {

    Organization findOrganizationById(Long searchId);

    Organization findOrganizationByName(String searchName);

    Page<Organization> findOrganizationByCountry(String country, Pageable pageable);

    List<Organization> findByOrganizationType(String searchType);

    Page<Organization> findByOrganizationType(String searchType, Pageable pageable);

    Page<Organization> findAll(Pageable pageable);

    List<Organization> findAll();

    Page<Organization> findOrganizationByCountryAndOrganizationType(String organizationCountry, String organizationType, Pageable pageable);

}
