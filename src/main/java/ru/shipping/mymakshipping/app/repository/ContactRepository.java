package ru.shipping.mymakshipping.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shipping.mymakshipping.app.entities.contact.Contact;

import java.util.List;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {

    Contact findContactById(Long searchId);

    Contact findContactByName(String searchName);

    List<Contact> findContactsByContactType(String searchType);

    List<Contact> findContactsByOrganizationId(long id);
}
