package ru.shipping.mymakshipping.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.shipping.mymakshipping.app.entities.role.Role;

@Repository
@RepositoryRestResource
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findRoleByRoleName(String name);

    Role findRoleByRoleId(long id);
}
