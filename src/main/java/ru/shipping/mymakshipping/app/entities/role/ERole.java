package ru.shipping.mymakshipping.app.entities.role;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
