package ru.shipping.mymakshipping.app.entities.contact;

public enum ContactType {
    MANAGER,
    ACCOUNTANT,
    SALES,
    OPERATION
}
