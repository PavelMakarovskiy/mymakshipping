package ru.shipping.mymakshipping.app.entities.contact;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import ru.shipping.mymakshipping.app.entities.additional.GeneralEntity;
import ru.shipping.mymakshipping.app.entities.organization.Organization;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table
public class Contact extends GeneralEntity {
    @Column(nullable = false, length = 50)
    @NotBlank(message = "Name of contact is mandatory!")
    @Length(max = 50, message = "Length of contact's name have to be less than 50 symbols.")
    private String name;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s)]){1}$)",
            message = "Phone number is invalid! Put from 7 till 20 digits, first symbol can be +, " +
                    "symbols () and - are accepted. Extension number can be added in brackets without letters, " +
                    "example: +7(812) 746 77 22 (506)")
    private String officePhoneNumber;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s]){1}$)",
            message = "Phone number is invalid! Put from 7 till 20 digits, first symbol can be +, " +
                    "symbols () and - are accepted.")
    private String mobilePhoneNumber;

    @Email(message = "Email format is invalid! Please point out correct email address.")
    private String email;

    @Column(length = 1024)
    @Length(max = 1024, message = "Comments have to be less than 1024 symbols.")
    private String comments;

    @Column(name = "contact_type", nullable = false)
    @NotBlank(message = "Contact's type is mandatory!")
    private String contactType;

    @Column(nullable = false)
    @NotBlank(message = "Contact's type is mandatory!")
    private String direction;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(columnDefinition = "organization_id")
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return getId() == contact.getId() && Objects.equals(name, contact.name) &&
                Objects.equals(officePhoneNumber, contact.officePhoneNumber) &&
                Objects.equals(mobilePhoneNumber, contact.mobilePhoneNumber) &&
                Objects.equals(email, contact.email) && Objects.equals(comments, contact.comments) &&
                contactType == contact.contactType && Objects.equals(organization, contact.organization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), name, officePhoneNumber, mobilePhoneNumber,
                email, comments, contactType, organization);
    }
}
