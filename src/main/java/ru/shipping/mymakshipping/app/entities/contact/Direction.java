package ru.shipping.mymakshipping.app.entities.contact;

public enum Direction {
    IMPORT_AND_EXPORT("IMPORT AND EXPORT"),
    IMPORT("IMPORT"),
    EXPORT("EXPORT");

    private String direction;

    Direction(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return direction;
    }
}
