package ru.shipping.mymakshipping.app.entities.organization;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import ru.shipping.mymakshipping.app.entities.additional.GeneralEntity;
import ru.shipping.mymakshipping.app.entities.contact.Contact;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table
public class Organization extends GeneralEntity {
    @Column(nullable = false, unique = true, length = 50)
    @NotBlank(message = "Name of organization is mandatory!")
    @Length(max = 50, message = "Length of organization name have to be less than 50 symbols.")
    private String name;

    @Column(nullable = false)
    @NotBlank(message = "Country is mandatory")
    private String country;

    @Column(name = "organization_type", nullable = false)
    @NotBlank(message = "Type of organization is mandatory!")
    private String organizationType;

    private String website;

    private String address;

    @Pattern(regexp = "(^$)|(^(\\s*)?(\\+)?([()\\s]?\\d[- _():]?){6,19}([\\d\\s]){1}$)",
            message = "Phone number is invalid! Put from 7 till 20 digits, first symbol can be +, symbols () and - are accepted.")
    private String phoneNumber;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organization", fetch = FetchType.EAGER)
    private List<Contact> contacts;

    @JsonIgnore
    public List<Contact> getContacts() {
        return contacts;
    }

    @Length(max = 1024, message = "Comments have to be less than 1024 symbols.")
    private String comments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return getId() == that.getId() && Objects.equals(name, that.name) &&
                Objects.equals(country, that.country) && organizationType == that.organizationType &&
                Objects.equals(website, that.website) && Objects.equals(address, that.address) &&
                Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(contacts, that.contacts) &&
                Objects.equals(comments, that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), name, country, organizationType, website, address, phoneNumber, contacts, comments);
    }
}
