package ru.shipping.mymakshipping.app.entities.additional;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Data
public class GeneralEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @CreatedDate
    @Temporal(TemporalType.DATE)
    @Column(name = "created")
    private Date created;

    @LastModifiedDate
    @Temporal(TemporalType.DATE)
    @Column(name = "updated")
    private Date updated;
}
