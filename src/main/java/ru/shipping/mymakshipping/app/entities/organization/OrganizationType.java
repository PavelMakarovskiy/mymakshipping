package ru.shipping.mymakshipping.app.entities.organization;

public enum OrganizationType {
    SHIPPING_LINE("SHIPPING LINE"),
    AIR_AGENT("AIR AGENT"),
    RO_RO_CARRIER("RO-RO CARRIER"),
    BREAK_BULK_OOG_FORWARDER("BREAK BULK / OOG FORWARDER"),
    LCL_OPERATOR("LCL OPERATOR"),
    CONTAINER_TRUCKER("CONTAINER TRUCKER"),
    TRUCKER("TRUCKER"),
    FORWARDER("FORWARDER"),
    RAILWAY("RAILWAY"),
    CUSTOMS_BROKER("CUSTOMS BROKER"),
    WAREHOUSE("WAREHOUSE"),
    TERMINAL("TERMINAL"),
    INSURANCE("INSURANCE COMPANY"),
    REEFER("REEFER"),
    SHIPOWNER("SHIPOWNER"),
    OTHER_ORGANIZATIONS("OTHER ORGANIZATIONS");

    private String type;

    OrganizationType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}


