package ru.shipping.mymakshipping.app.entities.role;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import ru.shipping.mymakshipping.app.entities.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
@Data
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private long roleId;

    @Column(name = "role_name")
    private String roleName;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private List<User> users;

    @Override
    public String toString() {
        return "Role{" +
                "roleId: " + this.getRoleId() + ", " +
                "roleName: " + roleName + "}";
    }
}


