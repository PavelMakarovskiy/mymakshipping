package ru.shipping.mymakshipping.app.entities.additional;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Country {
    private static List<String> countries;

    public static List<String> getCountries() {
        countries = new ArrayList<>();
        String[] locales = Locale.getISOCountries();
        for (String countryCode : locales) {
            Locale obj = new Locale("", countryCode);
            countries.add(obj.getDisplayCountry(Locale.ENGLISH));
            Collections.sort(countries);
        }
        return countries;
    }

}
