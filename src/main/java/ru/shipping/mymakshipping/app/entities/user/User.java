package ru.shipping.mymakshipping.app.entities.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import ru.shipping.mymakshipping.app.entities.additional.GeneralEntity;
import ru.shipping.mymakshipping.app.entities.role.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Table(name = "users")
@Entity
public class User extends GeneralEntity {
    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @Column
    private String activationCode;

    @Column(name = "status")
    private boolean isActive;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;
}
