package ru.shipping.mymakshipping.app.entities.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.shipping.mymakshipping.app.repository.RoleRepository;

import javax.annotation.PostConstruct;

@Component
public class RolesInitializer {
    private RoleRepository roleRepository;

    @Autowired
    public void setRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    private void init() {
        String userRoleName = ERole.ROLE_USER.toString();
        String adminRoleName = ERole.ROLE_ADMIN.toString();
        Role roleForUser = roleRepository.findRoleByRoleName(userRoleName);
        Role roleForAdmin = roleRepository.findRoleByRoleName(adminRoleName);
        if (roleForUser == null) {
            Role userRole = new Role();
            userRole.setRoleName(ERole.ROLE_USER.toString());
            roleRepository.save(userRole);
        }
        if (roleForAdmin == null) {
            Role userRole = new Role();
            userRole.setRoleName(ERole.ROLE_ADMIN.toString());
            roleRepository.save(userRole);
        }
    }
}
